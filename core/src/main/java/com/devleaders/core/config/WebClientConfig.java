package com.devleaders.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.reactive.function.client.WebClient;

import static com.devleaders.core.config.Profiles.MOCK_WEBSERVER;

@Configuration
public class WebClientConfig {

    @Bean
    @Profile("!" + MOCK_WEBSERVER)
    WebClient dataServiceWebClient(DataServiceProperties dataServiceProperties) {
        return WebClient.builder()
                .baseUrl(dataServiceProperties.getUrl())
                .build();
    }
}
