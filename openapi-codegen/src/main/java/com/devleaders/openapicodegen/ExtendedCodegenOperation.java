package com.devleaders.openapicodegen;

import lombok.EqualsAndHashCode;
import org.openapitools.codegen.CodegenOperation;

@EqualsAndHashCode(callSuper = true)
public class ExtendedCodegenOperation extends CodegenOperation {
    private boolean isCreated;
    private boolean hasServerWebExchange;

    ExtendedCodegenOperation(CodegenOperation codegenOperation) {
        this.responseHeaders.addAll(codegenOperation.responseHeaders);
        this.hasAuthMethods = codegenOperation.hasAuthMethods;
        this.hasConsumes = codegenOperation.hasConsumes;
        this.hasProduces = codegenOperation.hasProduces;
        this.hasParams = codegenOperation.hasParams;
        this.hasOptionalParams = codegenOperation.hasOptionalParams;
        this.hasRequiredParams = codegenOperation.hasRequiredParams;
        this.returnTypeIsPrimitive = codegenOperation.returnTypeIsPrimitive;
        this.returnSimpleType = codegenOperation.returnSimpleType;
        this.subresourceOperation = codegenOperation.subresourceOperation;
        this.isMapContainer = codegenOperation.isMapContainer;
        this.isListContainer = codegenOperation.isListContainer;
        this.isMultipart = codegenOperation.isMultipart;
        this.hasMore = codegenOperation.hasMore;
        this.isResponseBinary = codegenOperation.isResponseBinary;
        this.isResponseFile = codegenOperation.isResponseFile;
        this.hasReference = codegenOperation.hasReference;
        this.isRestfulIndex = codegenOperation.isRestfulIndex;
        this.isRestfulShow = codegenOperation.isRestfulShow;
        this.isRestfulCreate = codegenOperation.isRestfulCreate;
        this.isRestfulUpdate = codegenOperation.isRestfulUpdate;
        this.isRestfulDestroy = codegenOperation.isRestfulDestroy;
        this.isRestful = codegenOperation.isRestful;
        this.isDeprecated = codegenOperation.isDeprecated;
        this.isCallbackRequest = codegenOperation.isCallbackRequest;
        this.path = codegenOperation.path;
        this.operationId = codegenOperation.operationId;
        this.returnType = codegenOperation.returnType;
        this.httpMethod = codegenOperation.httpMethod;
        this.returnBaseType = codegenOperation.returnBaseType;
        this.returnContainer = codegenOperation.returnContainer;
        this.summary = codegenOperation.summary;
        this.unescapedNotes = codegenOperation.unescapedNotes;
        this.notes = codegenOperation.notes;
        this.baseName = codegenOperation.baseName;
        this.defaultResponse = codegenOperation.defaultResponse;
        this.discriminator = codegenOperation.discriminator;
        this.consumes = codegenOperation.consumes;
        this.produces = codegenOperation.produces;
        this.prioritizedContentTypes = codegenOperation.prioritizedContentTypes;
        this.bodyParam = codegenOperation.bodyParam;
        this.allParams = codegenOperation.allParams;
        this.bodyParams = codegenOperation.bodyParams;
        this.pathParams = codegenOperation.pathParams;
        this.queryParams = codegenOperation.queryParams;
        this.headerParams = codegenOperation.headerParams;
        this.formParams = codegenOperation.formParams;
        this.cookieParams = codegenOperation.cookieParams;
        this.requiredParams = codegenOperation.requiredParams;
        this.optionalParams = codegenOperation.optionalParams;
        this.authMethods = codegenOperation.authMethods;
        this.tags = codegenOperation.tags;
        this.responses = codegenOperation.responses;
        this.callbacks = codegenOperation.callbacks;
        this.imports = codegenOperation.imports;
        this.examples = codegenOperation.examples;
        this.requestBodyExamples = codegenOperation.requestBodyExamples;
        this.externalDocs = codegenOperation.externalDocs;
        this.vendorExtensions = codegenOperation.vendorExtensions;
        this.nickname = codegenOperation.nickname;
        this.operationIdOriginal = codegenOperation.operationIdOriginal;
        this.operationIdLowerCase = codegenOperation.operationIdLowerCase;
        this.operationIdCamelCase = codegenOperation.operationIdCamelCase;
        this.operationIdSnakeCase = codegenOperation.operationIdSnakeCase;
    }

    public boolean isCreated() {
        return isCreated;
    }

    void setCreated() {
        isCreated = true;
    }

    public boolean hasServerWebExchange() {
        return hasServerWebExchange;
    }

    public void setHasServerWebExchange() {
        this.hasServerWebExchange = true;
    }
}
