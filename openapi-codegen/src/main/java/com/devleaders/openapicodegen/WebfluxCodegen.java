package com.devleaders.openapicodegen;

import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.servers.Server;
import org.apache.commons.lang3.tuple.Pair;
import org.openapitools.codegen.CodegenModel;
import org.openapitools.codegen.CodegenOperation;
import org.openapitools.codegen.CodegenProperty;
import org.openapitools.codegen.CodegenResponse;
import org.openapitools.codegen.languages.SpringCodegen;
import org.openapitools.codegen.utils.StringUtils;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

public class WebfluxCodegen extends SpringCodegen {
    private static final String TEMPLATES_DIR = "api-templates";

    @Override
    public String getName() {
        return "Work tracker OpenAPI Codegen";
    }

    public WebfluxCodegen() {
        apiTemplateFiles.remove("api.mustache");
        embeddedTemplateDir = TEMPLATES_DIR + File.separator + "JavaSpring";
    }

    @Override
    public void processOpts() {
        super.processOpts();
        if (interfaceOnly) {
            apiTemplateFiles.put("webfluxRestApi.mustache", "RestApi.java");
        }
        importMapping.put("JsonInclude", "com.fasterxml.jackson.annotation.JsonInclude");
    }

    @Override
    public void postProcessModelProperty(CodegenModel model, CodegenProperty property) {
        super.postProcessModelProperty(model, property);
        model.imports.add("JsonInclude");
    }

    @Override
    public String toApiName(String name) {
        if (name.length() == 0) {
            return "DefaultApi";
        }
        name = sanitizeName(name);
        return StringUtils.camelize(name);
    }

    @Override
    public String findCommonPrefixOfVars(List<Object> vars) {
        return "";
    }


    @Override
    public CodegenOperation fromOperation(String path, String httpMethod, Operation operation, List<Server> servers) {
        CodegenOperation codegenOperation = super.fromOperation(path, httpMethod, operation, servers);
        return new ExtendedCodegenOperation(codegenOperation);
    }

    private final Consumer<Pair<String, ExtendedCodegenOperation>> setCustomHttpStatus = pair -> {
        String code = pair.getLeft();
        ExtendedCodegenOperation operation = pair.getRight();
        if (code.equals("201")) {
            operation.setCreated();
        }
    };

    @Override
    @SuppressWarnings("unchecked")
    public Map<String, Object> postProcessOperationsWithModels(Map<String, Object> objs, List<Object> allModels) {
        super.postProcessOperationsWithModels(objs, allModels);
        Set<String> customCodes = Set.of("201");
        Map<String, Object> operations = (Map<String, Object>) objs.get("operations");
        if (operations != null) {
            List<CodegenOperation> ops = (List<CodegenOperation>) operations.get("operation");
            ops.forEach(operation -> {
                if (Arrays.asList("authenticateUser", "exchangeToken").contains(operation.operationId)) {
                    ((ExtendedCodegenOperation) operation).setHasServerWebExchange();
                }
                List<CodegenResponse> responses = operation.responses;
                if (responses != null) {
                    responses.stream()
                            .filter(resp -> customCodes.contains(resp.code))
                            .map(resp -> Pair.of(resp.code, (ExtendedCodegenOperation) operation))
                            .findFirst()
                            .ifPresent(setCustomHttpStatus);
                }
            });
        }
        return objs;
    }
}
