package com.devleaders.integrationtests.config;

import com.github.tomakehurst.wiremock.common.ClasspathFileSource;
import com.github.tomakehurst.wiremock.common.FileSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.web.reactive.function.client.WebClient;

import static com.devleaders.core.config.Profiles.MOCK_WEBSERVER;

@Configuration
public class MockWebServerConfiguration {

    @Bean
    @Primary
    @Profile(MOCK_WEBSERVER)
    WebClient testWebClient(MockWebServer mockWebServer) {
        return WebClient.builder()
                .baseUrl("http://localhost:" + mockWebServer.getPort() + "/v1/api")
                .build();
    }

    @Bean
    @Profile(MOCK_WEBSERVER)
    FileSource fileSource() {
        return new ClasspathFileSource("wiremock");
    }
}
