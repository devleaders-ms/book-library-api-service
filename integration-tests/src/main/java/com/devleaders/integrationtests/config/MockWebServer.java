package com.devleaders.integrationtests.config;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.common.FileSource;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import static com.devleaders.core.config.Profiles.MOCK_WEBSERVER;
import static com.github.tomakehurst.wiremock.client.WireMock.configureFor;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

@Slf4j
@Component
@Profile(MOCK_WEBSERVER)
@RequiredArgsConstructor
public class MockWebServer {
    private WireMockServer wireMockServer;
    private final FileSource fileSource;

    @PostConstruct
    @SneakyThrows
    void startWireMockServer() {
        log.info("Starting mock web server...");
        wireMockServer = new WireMockServer(wireMockConfig()
                .dynamicPort()
                .fileSource(fileSource)
        );
        wireMockServer.start();
        configureFor(wireMockServer.port());
    }

    @PreDestroy
    @SneakyThrows
    void stopServer() {
        log.info("Stopping mock web server...");
        wireMockServer.stop();
    }

    public int getPort() {
        return wireMockServer.port();
    }

    public void clearRequestCount() {
        wireMockServer.resetRequests();
    }
}
