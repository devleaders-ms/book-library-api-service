package com.devleaders.integrationtests.rentals;

import com.devleaders.integrationtests.config.MockWebServer;
import com.devleaders.model.BookRental;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static com.devleaders.core.config.Profiles.MOCK_WEBSERVER;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@ActiveProfiles(MOCK_WEBSERVER)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureWebTestClient(timeout = "60000")
@DisplayName("Integration tests of GET /users/{id}/rentals")
public class GetRentalsByUserIdTest {

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private MockWebServer mockWebServer;

    @BeforeEach
    void setup() {
        mockWebServer.clearRequestCount();
    }

    @Test
    void shouldReturnRentalsWhenRentalsFound() {
        // given
        BookRental bookRental = BookRental.builder()
                .id(1L)
                .bookItemId(1L)
                .userId(1L)
                .rentDate(LocalDate.of(2020, 4, 26))
                .dueDate(LocalDate.of(2021, 4, 26))
                .build();

        // when & then
        webTestClient.get()
                .uri("/v1/api/users/1/rentals")
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.OK)
                .expectBody(BookRental[].class)
                .consumeWith(result -> assertBookRentals(List.of(bookRental), result));

        verify(getRequestedFor(urlEqualTo("/v1/api/users/1")));
        verify(getRequestedFor(urlEqualTo("/v1/api/bookRentals?userId=1")));
    }

    @Test
    void shouldReturnServerErrorWhenRentalsServiceError() {
        // when & then
        webTestClient.get()
                .uri("/v1/api/users/5/rentals")
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);

        verify(getRequestedFor(urlEqualTo("/v1/api/users/5")));
        verify(getRequestedFor(urlEqualTo("/v1/api/bookRentals?userId=5")));
    }

    @Test
    void shouldReturnNotFoundWhenUserNotFound() {
        // when & then
        webTestClient.get()
                .uri("/v1/api/users/2/rentals")
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.NOT_FOUND);

        verify(getRequestedFor(urlEqualTo("/v1/api/users/2")));
        verify(0, getRequestedFor(urlEqualTo("/v1/api/bookRentals?userId=2")));
    }

    @Test
    void shouldReturnServerErrorWhenUserServiceError() {
        // when & then
        webTestClient.get()
                .uri("/v1/api/users/4/rentals")
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);

        verify(getRequestedFor(urlEqualTo("/v1/api/users/4")));
        verify(0, getRequestedFor(urlEqualTo("/v1/api/bookRentals?userId=4")));
    }

    private void assertBookRentals(List<BookRental> expectedBookRentals, EntityExchangeResult<BookRental[]> result) {
        BookRental[] responseBody = result.getResponseBody();
        assertNotNull(responseBody);
        List<BookRental> bookRentals = Arrays.asList(responseBody);
        assertEquals(expectedBookRentals, bookRentals);
    }
}
