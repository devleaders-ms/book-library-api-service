package com.devleaders.integrationtests.bookItems;

import com.devleaders.integrationtests.config.MockWebServer;
import com.devleaders.model.BookItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.Arrays;
import java.util.List;

import static com.devleaders.core.config.Profiles.MOCK_WEBSERVER;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@ActiveProfiles(MOCK_WEBSERVER)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureWebTestClient
@DisplayName("Integration tests of GET /books/{bookId}/bookItems")
public class GetBookItemsByBookIdTest {

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private MockWebServer mockWebServer;

    @BeforeEach
    void setup() {
        mockWebServer.clearRequestCount();
    }

    @Test
    void shouldReturnOkWhenBookItemsFound() {
        // given
        BookItem bookItem = BookItem.builder()
                .id(1L)
                .bookId(1L)
                .isAvailable(true)
                .build();

        // when & then
        webTestClient.get()
                .uri("/v1/api/books/1/bookItems")
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.OK)
                .expectBody(BookItem[].class)
                .consumeWith(result -> assertBookItems(List.of(bookItem), result));

        verify(getRequestedFor(urlEqualTo("/v1/api/bookItems?bookId=1")));
    }

    @Test
    void shouldReturnNotFoundWhenBookItemsNotFound() {
        // when & then
        webTestClient.get()
                .uri("/v1/api/books/2/bookItems")
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.NOT_FOUND);

        verify(getRequestedFor(urlEqualTo("/v1/api/books/2")));
    }

    @Test
    void shouldReturnErrorWhenServiceError() {
        // when & then
        webTestClient.get()
                .uri("/v1/api/books/3/bookItems")
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);

        verify(getRequestedFor(urlEqualTo("/v1/api/books/3")));
    }

    private void assertBookItems(List<BookItem> expectedBookItems, EntityExchangeResult<BookItem[]> result) {
        BookItem[] responseBody = result.getResponseBody();
        assertNotNull(responseBody);
        List<BookItem> bookItems = Arrays.asList(responseBody);
        assertEquals(expectedBookItems, bookItems);
    }
}
