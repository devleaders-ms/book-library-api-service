package com.devleaders.integrationtests.auth;

import com.devleaders.integrationtests.config.MockWebServer;
import com.devleaders.model.JwtResponse;
import com.devleaders.model.LoginRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import static com.devleaders.core.config.Profiles.MOCK_WEBSERVER;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@ActiveProfiles(MOCK_WEBSERVER)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureWebTestClient
@DisplayName("Integration tests of POST /auth/login")
class LoginTest {

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private MockWebServer mockWebServer;

    @BeforeEach
    void setup() {
        mockWebServer.clearRequestCount();
    }

    @Test
    void shouldReturnUserWhenAuthenticationCorrect() {
        // given
        LoginRequest loginRequest = new LoginRequest("Mateusz", "123");

        // when & then
        webTestClient.post()
                .uri("/v1/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(loginRequest)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(JwtResponse.class)
                .consumeWith(this::assertJwtResponse);

        verify(getRequestedFor(urlEqualTo("/v1/api/users?login=Mateusz")));
    }

    @Test
    void shouldThrowExceptionWhenUserNotFound() {
        // given
        LoginRequest loginRequest = new LoginRequest("Jacek", "321");

        // when & then
        webTestClient.post()
                .uri("/v1/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(loginRequest)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.UNAUTHORIZED);

        verify(getRequestedFor(urlEqualTo("/v1/api/users?login=Jacek")));
    }

    @Test
    void shouldThrowUnknownErrorExceptionWhenUserEmpty() {
        // given
        LoginRequest loginRequest = new LoginRequest("", "");

        // when & then
        webTestClient.post()
                .uri("/v1/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(loginRequest)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);

        verify(getRequestedFor(urlEqualTo("/v1/api/users?login=")));
    }

    @Test
    void shouldThrowIncorrectPasswordExceptionWhenWrongPassword() {
        // given
        LoginRequest loginRequest = new LoginRequest("Zbyszek", "wrongPassword");

        // when & then
        webTestClient.post()
                .uri("/v1/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(loginRequest)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.UNAUTHORIZED);

        verify(getRequestedFor(urlEqualTo("/v1/api/users?login=Zbyszek")));
    }

    private void assertJwtResponse(EntityExchangeResult<JwtResponse> result) {
        JwtResponse responseBody = result.getResponseBody();
        assertNotNull(responseBody);
        assertEquals("bearer", responseBody.getTokenType());
        assertNotNull(responseBody.getAccessToken());
        assertEquals(1800, responseBody.getExpiresIn());
    }
}