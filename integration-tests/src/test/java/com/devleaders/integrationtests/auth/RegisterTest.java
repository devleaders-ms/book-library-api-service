package com.devleaders.integrationtests.auth;

import com.devleaders.integrationtests.config.MockWebServer;
import com.devleaders.model.RegisterRequest;
import com.devleaders.model.UserDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

import static com.devleaders.core.config.Profiles.MOCK_WEBSERVER;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@ActiveProfiles(MOCK_WEBSERVER)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureWebTestClient
@DisplayName("Integration tests of POST /auth/register")
class RegisterTest {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MockWebServer mockWebServer;

    @BeforeEach
    void setup() {
        mockWebServer.clearRequestCount();
    }

    @BeforeEach
    void init() {
        given(passwordEncoder.encode("password"))
                .willReturn("password");
    }

    @Test
    void whenUserNotFoundThenRegister() {
        // given
        final String login = "login1";
        RegisterRequest registerRequest = prepareRegisterRequest(login);
        UserDto userDto = prepareUserDto();

        // when & then
        webTestClient.post()
                .uri("/v1/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(registerRequest)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.CREATED)
                .expectBody(UserDto.class)
                .consumeWith(result -> assertEquals(userDto, result.getResponseBody()));

        verify(getRequestedFor(urlEqualTo("/v1/api/users?login=login1")));
    }

    @Test
    void whenUserFoundThenConflict() {
        // given
        RegisterRequest registerRequest = prepareRegisterRequest("login2");

        // when & then
        webTestClient.post()
                .uri("/v1/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(registerRequest)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.CONFLICT);

        verify(getRequestedFor(urlEqualTo("/v1/api/users?login=login2")));
    }

    @Test
    void whenDataServiceInternalServerErrorThenInternalServerError() {
        // given
        RegisterRequest registerRequest = prepareRegisterRequest("login3");

        // when & then
        webTestClient.post()
                .uri("/v1/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(registerRequest)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);

        verify(getRequestedFor(urlEqualTo("/v1/api/users?login=login3")));
    }

    private RegisterRequest prepareRegisterRequest(String login) {
        return new RegisterRequest(
                login,
                "password",
                "email@mail.com",
                "Jacek",
                "Balcerzak");
    }

    private UserDto prepareUserDto() {
        return UserDto.builder()
                .id(1L)
                .firstName("Jacek")
                .lastName("Balcerzak")
                .login("login1")
                .email("email@mail.com")
                .build();
    }
}