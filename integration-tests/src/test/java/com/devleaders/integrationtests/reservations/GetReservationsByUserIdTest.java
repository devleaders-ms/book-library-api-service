package com.devleaders.integrationtests.reservations;

import com.devleaders.integrationtests.config.MockWebServer;
import com.devleaders.model.Reservation;
import com.devleaders.model.ReservationStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static com.devleaders.core.config.Profiles.MOCK_WEBSERVER;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@ActiveProfiles(MOCK_WEBSERVER)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureWebTestClient(timeout = "60000")
@DisplayName("Integration tests of GET /users/{userId}/reservations")
class GetReservationsByUserIdTest {

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private MockWebServer mockWebServer;

    @BeforeEach
    void setup() {
        mockWebServer.clearRequestCount();
    }

    @Test
    void shouldReturnReservationsWhenExists() {
        // given
        Reservation reservation = new Reservation(1L, 1L, 1L, LocalDate.of(2020, 11, 11), ReservationStatus.AWAITING);

        // when & then
        webTestClient.get()
                .uri("/v1/api/users/{userId}/reservations", 1)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.OK)
                .expectBody(Reservation[].class)
                .consumeWith(result -> assertReservations(List.of(reservation), result));

        verify(1, getRequestedFor(urlEqualTo("/v1/api/reservations?userId=1")));
    }

    @Test
    void shouldReturnOkWhenNoReservations() {
        // when & then
        webTestClient.get()
                .uri("/v1/api/users/{userId}/reservations", 2)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.OK);

        verify(1, getRequestedFor(urlEqualTo("/v1/api/reservations?userId=2")));
    }

    @Test
    void shouldReturnNotFoundWhenUserNotFound() {
        // when & then
        webTestClient.get()
                .uri("/v1/api/users/{userId}/reservations", 3)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.NOT_FOUND);

        verify(1, getRequestedFor(urlEqualTo("/v1/api/reservations?userId=3")));
    }

    @Test
    void shouldReturnServerErrorWhenReservationsServiceError() {
        // when & then
        webTestClient.get()
                .uri("/v1/api/users/{userId}/reservations", 4)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);

        verify(1, getRequestedFor(urlEqualTo("/v1/api/reservations?userId=4")));
    }

    private void assertReservations(List<Reservation> expectedReservations, EntityExchangeResult<Reservation[]> result) {
        Reservation[] responseBody = result.getResponseBody();
        assertNotNull(responseBody);
        List<Reservation> reservations = Arrays.asList(responseBody);
        assertEquals(expectedReservations, reservations);
    }
}
