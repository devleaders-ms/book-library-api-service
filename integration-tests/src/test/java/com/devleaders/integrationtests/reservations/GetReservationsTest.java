package com.devleaders.integrationtests.reservations;

import com.devleaders.integrationtests.config.MockWebServer;
import com.devleaders.model.Reservation;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.Arrays;
import java.util.List;

import static com.devleaders.core.config.Profiles.MOCK_WEBSERVER;
import static com.devleaders.model.ReservationStatus.AWAITING;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.okJson;
import static com.github.tomakehurst.wiremock.client.WireMock.serverError;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@ActiveProfiles(MOCK_WEBSERVER)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureWebTestClient
@DisplayName("Integration tests of GET /reservations")
public class GetReservationsTest {

    @Autowired
    WebTestClient webTestClient;

    @Autowired
    MockWebServer mockWebServer;

    @BeforeEach
    void setup() {
        mockWebServer.clearRequestCount();
    }

    @Test
    void shouldReturnOkWhenGetReservations() throws JsonProcessingException {
        // given
        Reservation reservation = new Reservation(1L, 1L, 1L, null, AWAITING);
        String reservationJson = new ObjectMapper().writeValueAsString(singletonList(reservation));
        stubFor(get(urlEqualTo("/v1/api/reservations"))
                .willReturn(okJson(reservationJson)
                        .withHeader("Content-Type", "application/json")));

        // when & then
        webTestClient.get()
                .uri("/v1/api/reservations")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Reservation[].class)
                .consumeWith(response -> assertListContainsReservations(response, List.of(reservation)));

        verify(1, getRequestedFor(urlEqualTo("/v1/api/reservations")));
    }

    @Test
    void shouldReturnInternalServerErrorWhenServerError() {
        // given
        stubFor(get(urlEqualTo("/v1/api/reservations"))
                .willReturn(serverError()));

        // when & then
        webTestClient.get()
                .uri("/v1/api/reservations")
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);

        verify(1, getRequestedFor(urlEqualTo("/v1/api/reservations")));
    }

    private void assertListContainsReservations(EntityExchangeResult<Reservation[]> response, List<Reservation> expectedReservations) {
        Reservation[] responseBody = response.getResponseBody();
        assertNotNull(responseBody);
        List<Reservation> responseBooks = Arrays.asList(responseBody);
        assertEquals(expectedReservations, responseBooks);
    }
}
