package com.devleaders.integrationtests.users;

import com.devleaders.api.config.ApiAuthorizationManager;
import com.devleaders.integrationtests.config.MockWebServer;
import com.devleaders.model.UserData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import static com.devleaders.core.config.Profiles.MOCK_WEBSERVER;
import static com.github.tomakehurst.wiremock.client.WireMock.patchRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.apache.http.HttpHeaders.AUTHORIZATION;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@ActiveProfiles(MOCK_WEBSERVER)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureWebTestClient
@DisplayName("Integration tests of PATCH /users")
public class UserPatchTest {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private ApiAuthorizationManager apiAuthorizationManager;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MockWebServer mockWebServer;

    @BeforeEach
    void setup() {
        mockWebServer.clearRequestCount();
    }

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldReturnOkWhenPatchUser() {
        // given
        given(apiAuthorizationManager.check(any(), any()))
                .willReturn(Mono.just(new AuthorizationDecision(true)));
        given(passwordEncoder.encode("password"))
                .willReturn("password");
        UserData userData = prepareUserData();

        // when & then
        webTestClient.patch()
                .uri("/v1/api/users/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, "token")
                .bodyValue(userData)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.OK);

        verify(patchRequestedFor(urlEqualTo("/v1/api/users/1")));
    }

    @Test
    void shouldReturnNotFoundWhenUserNotFound() {
        // given
        given(apiAuthorizationManager.check(any(), any()))
                .willReturn(Mono.just(new AuthorizationDecision(true)));
        given(passwordEncoder.encode("password"))
                .willReturn("password");
        UserData userData = prepareUserData();

        // when & then
        webTestClient.patch()
                .uri("/v1/api/users/2")
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, "token")
                .bodyValue(userData)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.NOT_FOUND);

        verify(patchRequestedFor(urlEqualTo("/v1/api/users/2")));
    }

    @Test
    void shouldReturnServerErrorWhenServiceError() {
        // given
        given(apiAuthorizationManager.check(any(), any()))
                .willReturn(Mono.just(new AuthorizationDecision(true)));
        given(passwordEncoder.encode("password"))
                .willReturn("password");
        UserData userData = prepareUserData();

        // when & then
        webTestClient.patch()
                .uri("/v1/api/users/3")
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, "token")
                .bodyValue(userData)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);

        verify(patchRequestedFor(urlEqualTo("/v1/api/users/3")));
    }

    private UserData prepareUserData() {
        return new UserData("password", "email@mail.com");
    }

}
