package com.devleaders.integrationtests.books;

import com.devleaders.integrationtests.config.MockWebServer;
import com.devleaders.model.Author;
import com.devleaders.model.Book;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.devleaders.core.config.Profiles.MOCK_WEBSERVER;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@ActiveProfiles(MOCK_WEBSERVER)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureWebTestClient
@DisplayName("Integration tests of GET /book?authorName={authorName}")
public class GetBooksByAuthorTest {
    @Autowired
    WebTestClient webTestClient;

    @Autowired
    MockWebServer mockWebServer;

    @BeforeEach
    void setup() {
        mockWebServer.clearRequestCount();
    }

    @Test
    void shouldReturnBooksWhenAuthorExists() {
        // given
        Author author = new Author(1L, "Albert", "Camus");
        String authorName = author.getName() + " " + author.getLastName();
        Book book = new Book(
                1L,
                "The Plague",
                "978-83-06-03377-9",
                "Story of a plague sweeping the city of Oran.",
                "philosophical novel",
                298,
                null,
                Collections.singletonList(author)
        );

        // when & then
        webTestClient.get()
                .uri("/v1/api/books?author={authorName}", authorName)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Book[].class)
                .consumeWith(response -> assertListContainsBooks(response, List.of(book)));

        verify(getRequestedFor(urlEqualTo("/v1/api/books?authorName=Albert%20Camus")));
    }

    @Test
    void shouldReturnEmptyWhenAuthorDoesNotExist() {
        // given
        String author = "Bulgakov";

        // when & then
        webTestClient.get()
                .uri("/v1/api/books?author={author}", author)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Book[].class)
                .consumeWith(response -> assertListContainsBooks(response, Collections.emptyList()));

        verify(getRequestedFor(urlEqualTo("/v1/api/books?authorName=Bulgakov")));
    }

    @Test
    void shouldReturnAllBooksWhenNeitherAuthorNorTitleIsGiven() {
        // given
        Author author1 = new Author(1L, "Albert", "Camus");
        Author author2 = new Author(2L, "Mikhail", "Bulgakov");
        Book book1 = new Book(
                1L,
                "The Plague",
                "978-83-06-03377-9",
                "Story of a plague sweeping the city of Oran.",
                "philosophical novel",
                298,
                null,
                Collections.singletonList(author1)
        );
        Book book2 = new Book(
                2L,
                "The Master and Margarita",
                "0-14-118014-5",
                "The devil visits atheistic Soviet Union.",
                "fantasy, modernist",
                392,
                null,
                Collections.singletonList(author2)
        );

        // when & then
        webTestClient.get()
                .uri("/v1/api/books")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Book[].class)
                .consumeWith(response -> assertListContainsBooks(response, List.of(book1, book2)));

        verify(getRequestedFor(urlEqualTo("/v1/api/books")));
    }

    @Test
    void shouldReturnInternalServerErrorWhenServerError() {
        // given
        String author = "Witkacy";

        // when & then
        webTestClient.get()
                .uri("/v1/api/books?author={author}", author)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);

        verify(getRequestedFor(urlEqualTo("/v1/api/books?authorName=Witkacy")));
    }

    private void assertListContainsBooks(EntityExchangeResult<Book[]> response, List<Book> expectedBooks) {
        Book[] responseBody = response.getResponseBody();
        assertNotNull(responseBody);
        List<Book> responseBooks = Arrays.asList(responseBody);
        assertEquals(expectedBooks, responseBooks);
    }
}
