package com.devleaders.integrationtests.books;

import com.devleaders.integrationtests.config.MockWebServer;
import com.devleaders.model.Author;
import com.devleaders.model.Book;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.devleaders.core.config.Profiles.MOCK_WEBSERVER;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@ActiveProfiles(MOCK_WEBSERVER)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureWebTestClient
@DisplayName("Integration tests of GET /book?title={title}")
public class GetBooksByTitleTest {
    @Autowired
    WebTestClient webTestClient;

    @Autowired
    MockWebServer mockWebServer;

    @BeforeEach
    void setup() {
        mockWebServer.clearRequestCount();
    }

    @Test
    void shouldReturnBooksWhenTitleExists() {
        // given
        Author author = new Author(2L, "Wiley", "Brand");
        String title = "Java for Dummies";
        Book book = new Book(
                1L,
                "Java for Dummies",
                "101",
                "Java jest wszędzie, a rosnący popyt na aplikacje WWW i dla Androida sprawia, że programiści Javy są poszukiwani bardziej niż kiedykolwiek! Ten świetny podręcznik opisuje najważniejsze elementy tego języka, takie jak powłoka JShell. W książce znajdziesz też praktyczne pytania i ćwiczenia, które pomogą Ci rozwinąć umiejętności programowania w Javie. Dzięki prostym instrukcjom obsługiwania klas i metod języka Java, stosowania zmiennych oraz sterowania przepływem programu szybko staniesz się ekspertem programowania w Javie!",
                "IT",
                444,
                null,
                Collections.singletonList(author)
        );

        // when & then
        webTestClient.get()
                .uri("/v1/api/books?title={title}", title)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Book[].class)
                .consumeWith(response -> assertListContainsBooks(response, List.of(book)));

        verify(getRequestedFor(urlEqualTo("/v1/api/books?title=Java%20for%20Dummies")));
    }

    @Test
    void shouldReturnEmptyWhenAuthorDoesNotExist() {
        // given
        String title = "Harry Potter";

        // when & then
        webTestClient.get()
                .uri("/v1/api/books?title={title}", title)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Book[].class)
                .consumeWith(response -> assertListContainsBooks(response, Collections.emptyList()));

        verify(getRequestedFor(urlEqualTo("/v1/api/books?title=Harry%20Potter")));
    }

    @Test
    void shouldReturnBookWhenAuthorAndTitleIsGiven() {
        // given
        Author author = new Author(2L, "Wiley", "Brand");
        String title = "Java for Dummies";
        String authorName = author.getName() + " " + author.getLastName();

        Book book = new Book(
                1L,
                "Java for Dummies",
                "101",
                "Java jest wszędzie, a rosnący popyt na aplikacje WWW i dla Androida sprawia, że programiści Javy są poszukiwani bardziej niż kiedykolwiek! Ten świetny podręcznik opisuje najważniejsze elementy tego języka, takie jak powłoka JShell. W książce znajdziesz też praktyczne pytania i ćwiczenia, które pomogą Ci rozwinąć umiejętności programowania w Javie. Dzięki prostym instrukcjom obsługiwania klas i metod języka Java, stosowania zmiennych oraz sterowania przepływem programu szybko staniesz się ekspertem programowania w Javie!",
                "IT",
                444,
                null,
                Collections.singletonList(author)
        );

        // when & then
        webTestClient.get()
                .uri("/v1/api/books?author={authorName}&title={title}", authorName, title)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Book[].class)
                .consumeWith(response -> assertListContainsBooks(response, List.of(book)));

        verify(getRequestedFor(urlEqualTo("/v1/api/books?authorName=Wiley%20Brand&title=Java%20for%20Dummies")));
    }

    @Test
    void shouldReturnInternalServerErrorWhenServerError() {
        // given
        String title = "Java";

        // when & then
        webTestClient.get()
                .uri("/v1/api/books?title={title}", title)
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);

        verify(getRequestedFor(urlEqualTo("/v1/api/books?title=Java")));
    }

    private void assertListContainsBooks(EntityExchangeResult<Book[]> response, List<Book> expectedBooks) {
        Book[] responseBody = response.getResponseBody();
        assertNotNull(responseBody);
        List<Book> responseBooks = Arrays.asList(responseBody);
        assertEquals(expectedBooks, responseBooks);
    }

}