package com.devleaders.dataservice;

import com.devleaders.dataservice.domain.User;
import com.devleaders.dataservice.exception.DataServiceUnknownErrorException;
import com.devleaders.model.BookRental;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class UsersService {

    private final WebClient webClient;

    public Mono<User> getUserByLogin(String login) {
        return webClient.get()
                .uri("/users?login={login}", login)
                .exchange()
                .flatMap(this::handleClientResponse);
    }

    public Flux<BookRental> getBookRentalsByUserId(long userId) {
        return webClient.get()
                .uri("/bookRentals?userId={userId}", userId)
                .exchange()
                .flatMapMany(this::handleClientBookRentalResponseFlux);
    }

    public Flux<User> getUsersByFullNameLike(String fullName) {
        return webClient.get()
                .uri("/users?fullName={fullName}", fullName)
                .exchange()
                .flatMapMany(this::handleClientResponseFlux);
    }

    public Mono<User> getUserById(long id) {
        return webClient.get()
                .uri("/users/{id}", id)
                .exchange()
                .flatMap(this::handleClientResponse);
    }

    public Mono<User> save(User user) {
        return webClient.post()
                .uri("/users")
                .bodyValue(user)
                .exchange()
                .flatMap(clientResponse -> clientResponse.bodyToMono(User.class));
    }

    public Mono<User> patch(Map<String, String> userData, int userId) {
        return webClient.patch()
                .uri("/users/{userId}", userId)
                .bodyValue(userData)
                .exchange()
                .flatMap(this::handleClientResponse);
    }

    private Mono<User> handleClientResponse(ClientResponse response) {
        HttpStatus httpStatus = response.statusCode();
        Mono<User> mono;
        if (httpStatus == HttpStatus.OK) {
            mono = response.bodyToMono(User.class);
        } else if (httpStatus == HttpStatus.NOT_FOUND) {
            mono = Mono.empty();
        } else {
            mono = Mono.error(new DataServiceUnknownErrorException());
        }
        return mono;
    }

    private Flux<User> handleClientResponseFlux(ClientResponse response) {
        HttpStatus httpStatus = response.statusCode();
        Flux<User> flux;
        if (httpStatus == HttpStatus.OK) {
            flux = response.bodyToFlux(User.class);
        } else if (httpStatus == HttpStatus.NOT_FOUND) {
            flux = Flux.empty();
        } else {
            flux = Flux.error(new DataServiceUnknownErrorException());
        }
        return flux;
    }

    private Flux<BookRental> handleClientBookRentalResponseFlux(ClientResponse response) {
        HttpStatus httpStatus = response.statusCode();
        Flux<BookRental> flux;
        if (httpStatus == HttpStatus.OK) {
            flux = response.bodyToFlux(BookRental.class);
        } else if (httpStatus == HttpStatus.NOT_FOUND) {
            flux = Flux.empty();
        } else {
            flux = Flux.error(new DataServiceUnknownErrorException());
        }
        return flux;
    }

}
