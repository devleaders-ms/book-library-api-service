package com.devleaders.dataservice;

import com.devleaders.dataservice.exception.BookAlreadyExistsException;
import com.devleaders.dataservice.exception.DataServiceUnknownErrorException;
import com.devleaders.model.Book;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class BooksService {
    private final WebClient webClient;

    public Flux<Book> getAllBooks() {
        return webClient.get()
                .uri("/books")
                .exchange()
                .flatMapMany(this::handleResponseFlux);
    }

    public Flux<Book> getBooksByTitleLike(String titleName) {
        return webClient.get()
                .uri("/books?title={titleName}", titleName)
                .exchange()
                .flatMapMany(this::handleResponseFlux);
    }

    public Flux<Book> getBooksByAuthor(String authorName) {
        return webClient.get()
                .uri("/books?authorName={authorName}", authorName)
                .exchange()
                .flatMapMany(this::handleResponseFlux);
    }

    public Flux<Book> getBooksByAuthorAndTitle(String authorName, String titleName) {
        return webClient.get()
                .uri("/books?authorName={authorName}&title={titleName}", authorName, titleName)
                .exchange()
                .flatMapMany(this::handleResponseFlux);
    }

    public Mono<Book> getBookById(long bookId) {
        return webClient.get()
                .uri("/books/{bookId}", bookId)
                .exchange()
                .flatMap(this::handleResponse);
    }

    public Mono<Book> createBook(Book book) {
        return webClient.post()
                .uri("/books")
                .bodyValue(book)
                .exchange()
                .flatMap(this::handleResponse);
    }

    private Mono<Book> handleResponse(ClientResponse clientResponse) {
        HttpStatus httpStatus = clientResponse.statusCode();
        Mono<Book> mono;
        if (httpStatus == HttpStatus.OK) {
            mono = clientResponse.bodyToMono(Book.class);
        } else if (httpStatus == HttpStatus.NOT_FOUND) {
            mono = Mono.empty();
        } else if (httpStatus == HttpStatus.CONFLICT) {
            mono = Mono.error(new BookAlreadyExistsException());
        } else {
            mono = Mono.error(new DataServiceUnknownErrorException());
        }
        return mono;
    }

    private Flux<Book> handleResponseFlux(ClientResponse clientResponse) {
        HttpStatus httpStatus = clientResponse.statusCode();
        Flux<Book> flux;
        if (httpStatus == HttpStatus.OK) {
            flux = clientResponse.bodyToFlux(Book.class);
        } else if (httpStatus == HttpStatus.NOT_FOUND) {
            flux = Flux.empty();
        } else {
            flux = Flux.error(new DataServiceUnknownErrorException());
        }
        return flux;
    }

}
