package com.devleaders.dataservice.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum RoleType {
    ROLE_USER("USER"),
    ROLE_LIBRARIAN("LIBRARIAN"),
    ROLE_ADMIN("ADMIN");

    private final String role;
}
