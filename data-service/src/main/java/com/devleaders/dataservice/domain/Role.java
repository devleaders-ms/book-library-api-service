package com.devleaders.dataservice.domain;

import lombok.Data;

@Data
public class Role {
    private long id;
    private RoleType roleType;
}
