package com.devleaders.dataservice;

import com.devleaders.dataservice.exception.DataServiceUnknownErrorException;
import com.devleaders.dataservice.exception.DataServiceUserNotFoundException;
import com.devleaders.model.Reservation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@Service
@RequiredArgsConstructor
public class ReservationsService {

    private final WebClient webClient;

    public Flux<Reservation> getReservations() {
        return webClient
                .get()
                .uri("/reservations")
                .exchange()
                .flatMapMany(this::handleResponseFlux);
    }

    public Flux<Reservation> getReservationsByUserId(long userId) {
        return webClient
                .get()
                .uri("/reservations?userId={userId}", userId)
                .exchange()
                .flatMapMany(this::handleResponseFlux);
    }

    private Flux<Reservation> handleResponseFlux(ClientResponse clientResponse) {
        HttpStatus httpStatus = clientResponse.statusCode();
        Flux<Reservation> flux;
        if (httpStatus == HttpStatus.OK) {
            flux = clientResponse.bodyToFlux(Reservation.class);
        } else if (httpStatus == HttpStatus.NOT_FOUND) {
            flux = Flux.error(new DataServiceUserNotFoundException());
        } else {
            flux = Flux.error(new DataServiceUnknownErrorException());
        }
        return flux;
    }

}
