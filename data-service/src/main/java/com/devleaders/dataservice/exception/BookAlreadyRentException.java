package com.devleaders.dataservice.exception;

public class BookAlreadyRentException extends RuntimeException {
    public BookAlreadyRentException() {
        super("Oops! Looks like this book is already rent!");
    }
}