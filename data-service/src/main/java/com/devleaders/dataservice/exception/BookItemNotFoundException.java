package com.devleaders.dataservice.exception;

public class BookItemNotFoundException extends RuntimeException {
    public BookItemNotFoundException() {
        super("BookItem does not exist exception");
    }
}
