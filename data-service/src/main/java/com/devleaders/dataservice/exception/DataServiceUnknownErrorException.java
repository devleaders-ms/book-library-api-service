package com.devleaders.dataservice.exception;

public class DataServiceUnknownErrorException extends RuntimeException {
    public DataServiceUnknownErrorException() {
        super("Oops! Something went wrong!");
    }
}
