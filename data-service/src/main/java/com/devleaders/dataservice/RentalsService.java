package com.devleaders.dataservice;

import com.devleaders.dataservice.exception.BookAlreadyRentException;
import com.devleaders.dataservice.exception.DataServiceUnknownErrorException;
import com.devleaders.model.BookRental;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class RentalsService {

    private final WebClient webClient;

    public Mono<BookRental> rentBook(long bookItemId, long userId) {
        return Mono.just(prepareNewBookRental(bookItemId, userId))
                .flatMap(this::createBookRental);
    }

    private BookRental prepareNewBookRental(long bookItemId, long userId) {
        LocalDate now = LocalDate.now();

        return BookRental.builder()
                .bookItemId(bookItemId)
                .userId(userId)
                .rentDate(now)
                .dueDate(now.plusDays(14L))
                .build();
    }

    private Mono<BookRental> createBookRental(BookRental bookRental) {
        return webClient.post()
                .uri("/bookRentals")
                .bodyValue(bookRental)
                .exchange()
                .flatMap(this::handleClientResponse);
    }

    private Mono<BookRental> handleClientResponse(ClientResponse response) {
        HttpStatus httpStatus = response.statusCode();
        Mono<BookRental> mono;
        if (httpStatus == HttpStatus.OK || httpStatus == HttpStatus.CREATED) {
            mono = response.bodyToMono(BookRental.class);
        } else if (httpStatus == HttpStatus.NOT_FOUND) {
            mono = Mono.empty();
        } else if (httpStatus == HttpStatus.CONFLICT) {
            mono = Mono.error(new BookAlreadyRentException());
        } else {
            mono = Mono.error(new DataServiceUnknownErrorException());
        }
        return mono;
    }
}