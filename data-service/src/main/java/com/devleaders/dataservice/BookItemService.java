package com.devleaders.dataservice;

import com.devleaders.dataservice.exception.BookAlreadyRentException;
import com.devleaders.dataservice.exception.BookItemNotFoundException;
import com.devleaders.dataservice.exception.DataServiceBookNotFoundException;
import com.devleaders.dataservice.exception.DataServiceUnknownErrorException;
import com.devleaders.model.BookItem;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class BookItemService {

    private final WebClient webClient;

    private Mono<BookItem> getBookItem(long id) {
        return webClient.get()
                .uri("/bookItems/{id}", id)
                .exchange()
                .flatMap(this::handleClientResponse);
    }

    private Mono<BookItem> updateBookItemAvailability(long id) {
        Map<String, Boolean> status = Map.of("status", false);

        return webClient.patch()
                .uri("/bookItems/{id}", id)
                .bodyValue(status)
                .exchange()
                .flatMap(this::handleClientResponse);
    }

    public Flux<BookItem> getBookItemsForBookId(long id) {
        return webClient.get()
                .uri("/bookItems?bookId={id}", id)
                .exchange()
                .flatMapMany(this::handleResponseFlux);
    }

    public Mono<BookItem> changeBookItemAvailability(long id) {
        return getBookItem(id)
                .switchIfEmpty(Mono.error(BookItemNotFoundException::new))
                .filter(BookItem::getIsAvailable)
                .switchIfEmpty(Mono.error(BookAlreadyRentException::new))
                .flatMap(bookItem -> updateBookItemAvailability(bookItem.getId()))
                .switchIfEmpty(Mono.error(BookAlreadyRentException::new));
    }

    public Flux<BookItem> getAvailableBookItems(long bookId) {
        return webClient.get()
                .uri("/bookItems?bookId={bookId}&isAvailable={isAvailable}", bookId, "true")
                .exchange()
                .flatMapMany(this::handleResponseFlux);
    }

    private Mono<BookItem> handleClientResponse(ClientResponse response) {
        HttpStatus httpStatus = response.statusCode();
        Mono<BookItem> mono;
        if (httpStatus == HttpStatus.OK) {
            mono = response.bodyToMono(BookItem.class);
        } else if (httpStatus == HttpStatus.NOT_FOUND) {
            mono = Mono.empty();
        } else if (httpStatus == HttpStatus.CONFLICT) {
            mono = Mono.error(new BookAlreadyRentException());
        } else {
            mono = Mono.error(new DataServiceUnknownErrorException());
        }

        return mono;
    }

    private Flux<BookItem> handleResponseFlux(ClientResponse clientResponse) {
        HttpStatus httpStatus = clientResponse.statusCode();
        Flux<BookItem> flux;
        if (httpStatus == HttpStatus.OK) {
            flux = clientResponse.bodyToFlux(BookItem.class);
        } else if (httpStatus == HttpStatus.NOT_FOUND) {
            flux = Flux.error(new DataServiceBookNotFoundException());
        } else {
            flux = Flux.error(new DataServiceUnknownErrorException());
        }
        return flux;
    }

}