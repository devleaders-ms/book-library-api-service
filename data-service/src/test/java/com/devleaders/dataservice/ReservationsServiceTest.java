package com.devleaders.dataservice;

import com.devleaders.dataservice.exception.DataServiceUnknownErrorException;
import com.devleaders.model.Reservation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ReservationsServiceTest {

    @Mock
    private WebClient webClient;

    @InjectMocks
    private ReservationsService reservationsService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldReturnReservationsWhenExists() {
        // given
        ClientResponse clientResponse = mockWebClientResponse(
                webClient,
                "/reservations"
        );
        Reservation reservation = new Reservation();
        given(clientResponse.bodyToFlux(Reservation.class))
                .willReturn(Flux.just(reservation));
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.OK);

        // when & then
        StepVerifier.create(reservationsService.getReservations())
                .expectNext(reservation)
                .verifyComplete();

        verify(clientResponse).statusCode();
        verify(clientResponse).bodyToFlux(Reservation.class);
    }

    @Test
    void shouldReturnReservationsForUserWhenExists() {
        // given
        ClientResponse clientResponse = mockWebClientResponse(
                webClient,
                "/reservations?userId={userId}",
                1L
        );
        Reservation reservation = new Reservation();
        given(clientResponse.bodyToFlux(Reservation.class))
                .willReturn(Flux.just(reservation));
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.OK);

        // when & then
        StepVerifier.create(reservationsService.getReservationsByUserId(1L))
                .expectNext(reservation)
                .verifyComplete();

        verify(clientResponse).statusCode();
        verify(clientResponse).bodyToFlux(Reservation.class);
    }

    @Test
    void shouldReturnEmptyWhenNoReservationsExists() {
        // given
        ClientResponse clientResponse = mockWebClientResponse(
                webClient,
                "/reservations"
        );
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.OK);
        given(clientResponse.bodyToFlux(Reservation.class))
                .willReturn(Flux.empty());

        // when & then
        StepVerifier.create(reservationsService.getReservations())
                .verifyComplete();

        verify(clientResponse).statusCode();
    }

    @Test
    void shouldThrowUnknownErrorExceptionWhenErrorInService() {
        // given
        ClientResponse clientResponse = mockWebClientResponse(
                webClient,
                "/reservations"
        );
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.INTERNAL_SERVER_ERROR);

        // when & then
        StepVerifier.create(reservationsService.getReservations())
                .expectErrorMatches(throwable -> throwable instanceof DataServiceUnknownErrorException
                        && throwable.getMessage().equals("Oops! Something went wrong!"))
                .verify();

        verify(clientResponse).statusCode();
    }

    public ClientResponse mockWebClientResponse(WebClient webClient,
                                                String uri,
                                                Object... uriVariables) {
        final var clientResponse = mock(ClientResponse.class);
        final var requestHeadersUriSpec = mock(WebClient.RequestHeadersUriSpec.class);

        when(webClient.get())
                .thenReturn(requestHeadersUriSpec);
        Optional.ofNullable(uriVariables)
                .ifPresentOrElse(variables -> {
                            final var requestHeadersSpec = mock(WebClient.RequestHeadersSpec.class);
                            when(requestHeadersUriSpec.uri(uri, variables))
                                    .thenReturn(requestHeadersSpec);
                            when(requestHeadersSpec.exchange())
                                    .thenReturn(Mono.just(clientResponse));
                        },
                        () -> when(requestHeadersUriSpec.exchange())
                                .thenReturn(Mono.just(clientResponse)));

        return clientResponse;
    }
}