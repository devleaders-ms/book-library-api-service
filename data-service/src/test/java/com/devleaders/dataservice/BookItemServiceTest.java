package com.devleaders.dataservice;

import com.devleaders.dataservice.exception.BookAlreadyRentException;
import com.devleaders.dataservice.exception.BookItemNotFoundException;
import com.devleaders.dataservice.exception.DataServiceUnknownErrorException;
import com.devleaders.model.BookItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDate;
import java.util.Map;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class BookItemServiceTest {

    @Mock
    private WebClient webClient;

    @InjectMocks
    private BookItemService bookItemService;

    private BookItem bookItem;
    private ClientResponse clientResponse;

    @BeforeEach
    private void init() {
        MockitoAnnotations.initMocks(this);
        clientResponse = mock(ClientResponse.class);
        bookItem = new BookItem();
        mockWebClient();
    }

    @Test
    void shouldThrowBookItemNotFoundExceptionWhenBookItemDoesNotExist() {
        // given
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.NOT_FOUND);

        // when & then
        StepVerifier.create(bookItemService.changeBookItemAvailability(1L))
                .expectErrorMatches(throwable -> throwable instanceof BookItemNotFoundException
                        && throwable.getMessage().equals("BookItem does not exist exception"))
                .verify();

        verify(clientResponse).statusCode();
    }

    @Test
    void shouldThrowBookAlreadyRentExceptionWhenBookItemNotAvailableToRent() {
        // given
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.OK);
        given(clientResponse.bodyToMono(BookItem.class))
                .willReturn(Mono.just(getBookItemWithGivenStatus(false)));

        // when & then
        StepVerifier.create(bookItemService.changeBookItemAvailability(1L))
                .expectErrorMatches(throwable -> throwable instanceof BookAlreadyRentException
                        && throwable.getMessage().equals("Oops! Looks like this book is already rent!"))
                .verify();

        verify(clientResponse).statusCode();
        verify(clientResponse).bodyToMono(BookItem.class);
    }

    @Test
    void shouldThrowBookAlreadyRentExceptionWhenBookItemAlreadyRent() {
        // given
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.CONFLICT);

        // when & then
        StepVerifier.create(bookItemService.changeBookItemAvailability(1L))
                .expectErrorMatches(throwable -> throwable instanceof BookAlreadyRentException
                        && throwable.getMessage().equals("Oops! Looks like this book is already rent!"))
                .verify();

        verify(clientResponse).statusCode();
    }

    @Test
    void shouldThrowDataServiceUnknownErrorExceptionWhenOtherStatus() {
        // given
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.I_AM_A_TEAPOT);

        // when & then
        StepVerifier.create(bookItemService.changeBookItemAvailability(1L))
                .expectErrorMatches(throwable -> throwable instanceof DataServiceUnknownErrorException
                        && throwable.getMessage().equals("Oops! Something went wrong!"))
                .verify();

        verify(clientResponse).statusCode();
    }

    @Test
    void shouldGetBookItemWithChangedStatus() {
        // given
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.OK);

        when(clientResponse.bodyToMono(BookItem.class))
                .thenReturn(
                        Mono.just(getBookItemWithGivenStatus(true)),
                        Mono.just(getBookItemWithGivenStatus(false))
                );

        // when & then
        StepVerifier.create(bookItemService.changeBookItemAvailability(1L))
                .expectNext(getBookItemWithGivenStatus(false))
                .verifyComplete();
    }

    @Test
    void shouldReturnAvailableBookItemsWhenExists() {
        // given
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.OK);

        when(clientResponse.bodyToFlux(BookItem.class))
                .thenReturn(Flux.just(bookItem));

        // when & then
        StepVerifier.create(bookItemService.getAvailableBookItems(1))
                .expectNext(bookItem)
                .verifyComplete();

        verify(clientResponse, times(1)).statusCode();
    }

    @Test
    void shouldReturnEmptyWhenAvailableBookItemsNotExists() {
        // given
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.OK);

        when(clientResponse.bodyToFlux(BookItem.class))
                .thenReturn(Flux.empty());

        // when & then
        StepVerifier.create(bookItemService.getAvailableBookItems(1))
                .verifyComplete();

        verify(clientResponse, times(1)).statusCode();
    }

    @Test
    void shouldThrowDataServiceUnknownErrorExceptionWhenBookItemsServiceError() {
        // given
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.INTERNAL_SERVER_ERROR);

        // when & then
        StepVerifier.create(bookItemService.getAvailableBookItems(1))
                .expectErrorMatches(throwable -> throwable instanceof DataServiceUnknownErrorException
                        && throwable.getMessage().equals("Oops! Something went wrong!"))
                .verify();

        verify(clientResponse, times(1)).statusCode();
    }

    private void mockWebClient() {
        WebClient.RequestHeadersUriSpec requestHeadersUriSpec = mock(WebClient.RequestHeadersUriSpec.class);
        WebClient.RequestHeadersSpec requestHeadersSpec = mock(WebClient.RequestHeadersSpec.class);

        when(webClient.get())
                .thenReturn(requestHeadersUriSpec);
        when(requestHeadersUriSpec.uri("/bookItems/{id}", 1L))
                .thenReturn(requestHeadersSpec);
        when(requestHeadersSpec.exchange())
                .thenReturn(Mono.just(clientResponse));

        WebClient.RequestBodyUriSpec requestBodyUriSpec = mock(WebClient.RequestBodyUriSpec.class);
        WebClient.RequestBodySpec requestBodySpec = mock(WebClient.RequestBodySpec.class);
        Map<String, Boolean> status = Map.of("status", false);

        when(webClient.patch())
                .thenReturn(requestBodyUriSpec);
        when(requestBodyUriSpec.uri("/bookItems/{id}", 1L))
                .thenReturn(requestBodySpec);
        when(requestBodySpec.bodyValue(status))
                .thenReturn(requestHeadersSpec);

        when(webClient.get())
                .thenReturn(requestHeadersUriSpec);
        when(requestHeadersUriSpec.uri("/bookItems?bookId={bookId}&isAvailable={isAvailable}", 1L, "true"))
                .thenReturn(requestHeadersSpec);
        when(requestHeadersSpec.exchange())
                .thenReturn(Mono.just(clientResponse));
    }

    private BookItem getBookItemWithGivenStatus(boolean status) {
        return BookItem.builder()
                .id(1L)
                .bookId(1L)
                .sinceWhenInLibraryDate(LocalDate.now())
                .isAvailable(status)
                .build();
    }
}