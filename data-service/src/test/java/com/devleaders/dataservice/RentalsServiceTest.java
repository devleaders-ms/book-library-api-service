package com.devleaders.dataservice;

import com.devleaders.dataservice.exception.BookAlreadyRentException;
import com.devleaders.dataservice.exception.DataServiceUnknownErrorException;
import com.devleaders.model.BookRental;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDate;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class RentalsServiceTest {

    @Mock
    private WebClient webClient;

    @InjectMocks
    private RentalsService rentalsService;

    private BookRental bookRental;
    private ClientResponse clientResponse;

    @BeforeEach
    private void init() {
        MockitoAnnotations.initMocks(this);
        bookRental = getBookRental();
        mockClientResponse();
        mockWebClient();
    }

    @Test
    void ShouldReturnBookRentalWithGivenBookItem() {
        // given
        long bookItemId = 1L;
        long userId = 1L;

        given(clientResponse.statusCode())
                .willReturn(HttpStatus.OK);
        given(clientResponse.bodyToMono(BookRental.class))
                .willReturn(Mono.just(bookRental));

        // when & then
        StepVerifier.create(rentalsService.rentBook(bookItemId, userId))
                .expectNext(bookRental)
                .verifyComplete();

        verify(clientResponse).statusCode();
        verify(clientResponse).bodyToMono(BookRental.class);
    }

    @Test
    void ShouldReturnMonoEmptyWhenStatusNotFound() {
        // given
        long bookItemId = 1L;
        long userId = 1L;

        given(clientResponse.statusCode())
                .willReturn(HttpStatus.NOT_FOUND);

        // when & then
        StepVerifier.create(rentalsService.rentBook(bookItemId, userId))
                .verifyComplete();

        verify(clientResponse).statusCode();
    }

    @Test
    void ShouldThrowBookAlreadyRentExceptionWhenRentalsNotFound() {
        // given
        long bookItemId = 1L;
        long userId = 1L;

        given(clientResponse.statusCode())
                .willReturn(HttpStatus.CONFLICT);

        // when & then
        StepVerifier.create(rentalsService.rentBook(bookItemId, userId))
                .expectErrorMatches(throwable -> throwable instanceof BookAlreadyRentException
                        && throwable.getMessage().equals("Oops! Looks like this book is already rent!"))
                .verify();

        verify(clientResponse).statusCode();
    }

    @Test
    void ShouldThrowDataServiceUnknownErrorExceptionWhenOtherStatus() {
        // given
        long bookItemId = 1L;
        long userId = 1L;

        given(clientResponse.statusCode())
                .willReturn(HttpStatus.I_AM_A_TEAPOT);

        // when & then
        StepVerifier.create(rentalsService.rentBook(bookItemId, userId))
                .expectErrorMatches(throwable -> throwable instanceof DataServiceUnknownErrorException
                        && throwable.getMessage().equals("Oops! Something went wrong!"))
                .verify();

        verify(clientResponse).statusCode();
    }

    private void mockWebClient() {
        WebClient.RequestBodyUriSpec requestBodyUriSpec = mock(WebClient.RequestBodyUriSpec.class);
        WebClient.RequestBodySpec requestBodySpec = mock(WebClient.RequestBodySpec.class);
        WebClient.RequestHeadersSpec requestHeadersSpec = mock(WebClient.RequestHeadersSpec.class);

        when(webClient.post())
                .thenReturn(requestBodyUriSpec);
        when(requestBodyUriSpec.uri("/bookRentals"))
                .thenReturn(requestBodySpec);
        when(requestBodySpec.bodyValue(bookRental))
                .thenReturn(requestHeadersSpec);
        when(requestHeadersSpec.exchange())
                .thenReturn(Mono.just(clientResponse));
    }

    private void mockClientResponse() {
        Mono<BookRental> bookRentalMono = Mono.just(bookRental);
        clientResponse = mock(ClientResponse.class);
        when(clientResponse.bodyToMono(BookRental.class))
                .thenReturn(bookRentalMono);
    }

    private BookRental getBookRental() {
        LocalDate now = LocalDate.now();
        return BookRental.builder()
                .bookItemId(1L)
                .userId(1L)
                .rentDate(now)
                .dueDate(now.plusDays(14L))
                .build();
    }
}