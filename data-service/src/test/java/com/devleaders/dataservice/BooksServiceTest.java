package com.devleaders.dataservice;

import com.devleaders.dataservice.exception.DataServiceUnknownErrorException;
import com.devleaders.model.Book;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class BooksServiceTest {

    @Mock
    private WebClient webClient;

    @InjectMocks
    private BooksService booksService;
    private Book book = new Book();

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldReturnAllBooksWhenExists() {
        // given
        ClientResponse clientResponse = mockWebClientResponse(
                webClient,
                "/books"
        );
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.OK);
        given(clientResponse.bodyToFlux(Book.class))
                .willReturn(Flux.just(book));

        // when & then
        StepVerifier.create(booksService.getAllBooks())
                .expectNext(book)
                .verifyComplete();

        verify(clientResponse).statusCode();
        verify(clientResponse).bodyToFlux(Book.class);
    }

    @Test
    void shouldReturnEmptyWhenNoBookExists() {
        // given
        ClientResponse clientResponse = mockWebClientResponse(
                webClient,
                "/books"
        );
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.NOT_FOUND);

        // when & then
        StepVerifier.create(booksService.getAllBooks())
                .verifyComplete();

        verify(clientResponse).statusCode();
    }

    @Test
    void shouldReturnBooksWhenTitleExists() {
        // given
        String titleName = "The Plague";
        ClientResponse clientResponse = mockWebClientResponse(
                webClient,
                "/books?title={titleName}",
                titleName
        );
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.OK);
        given(clientResponse.bodyToFlux(Book.class))
                .willReturn(Flux.just(book));

        // when & then
        StepVerifier.create(booksService.getBooksByTitleLike(titleName))
                .expectNext(book)
                .verifyComplete();

        verify(clientResponse).statusCode();
        verify(clientResponse).bodyToFlux(Book.class);
    }

    @Test
    void shouldReturnEmptyWhenTitleDoesNotExist() {
        // given
        String titleName = "The Plague";
        ClientResponse clientResponse = mockWebClientResponse(
                webClient,
                "/books?title={titleName}",
                titleName
        );
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.NOT_FOUND);

        // when & then
        StepVerifier.create(booksService.getBooksByTitleLike(titleName))
                .verifyComplete();

        verify(clientResponse).statusCode();
    }

    @Test
    void shouldThrowUnknownErrorExceptionWhenErrorWhileFindingBookByTitle() {
        // given
        String titleName = "The Plague";
        ClientResponse clientResponse = mockWebClientResponse(
                webClient,
                "/books?title={titleName}",
                titleName
        );
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.INTERNAL_SERVER_ERROR);

        // when & then
        StepVerifier.create(booksService.getBooksByTitleLike(titleName))
                .expectErrorMatches(throwable -> throwable instanceof DataServiceUnknownErrorException
                        && throwable.getMessage().equals("Oops! Something went wrong!"))
                .verify();

        verify(clientResponse).statusCode();
    }

    @Test
    void shouldReturnBooksWhenAuthorExists() {
        // given
        String authorName = "Adam Mickiewicz";
        ClientResponse clientResponse = mockWebClientResponse(
                webClient,
                "/books?authorName={authorName}",
                authorName
        );
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.OK);
        given(clientResponse.bodyToFlux(Book.class))
                .willReturn(Flux.just(book));

        // when & then
        StepVerifier.create(booksService.getBooksByAuthor(authorName))
                .expectNext(book)
                .verifyComplete();

        verify(clientResponse).statusCode();
        verify(clientResponse).bodyToFlux(Book.class);
    }

    @Test
    void shouldReturnEmptyWhenAuthorDoesNotExist() {
        // given
        String authorName = "Adam Mickiewicz";
        ClientResponse clientResponse = mockWebClientResponse(
                webClient,
                "/books?authorName={authorName}",
                authorName
        );
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.NOT_FOUND);

        // when & then
        StepVerifier.create(booksService.getBooksByAuthor(authorName))
                .verifyComplete();

        verify(clientResponse).statusCode();
    }

    @Test
    void shouldThrowUnknownErrorExceptionWhenErrorWhileFindingBookByAuthor() {
        // given
        String authorName = "Adam Mickiewicz";
        ClientResponse clientResponse = mockWebClientResponse(
                webClient,
                "/books?authorName={authorName}",
                authorName
        );
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.INTERNAL_SERVER_ERROR);

        // when & then
        StepVerifier.create(booksService.getBooksByAuthor(authorName))
                .expectErrorMatches(throwable -> throwable instanceof DataServiceUnknownErrorException
                        && throwable.getMessage().equals("Oops! Something went wrong!"))
                .verify();

        verify(clientResponse).statusCode();
    }

    @Test
    void shouldReturnBooksWhenAuthorAndTitleExists() {
        // given
        String authorName = "Adam Mickiewicz";
        String titleName = "Dziady";
        ClientResponse clientResponse = mockWebClientResponse(
                webClient,
                "/books?authorName={authorName}&title={titleName}",
                authorName, titleName
        );
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.OK);
        given(clientResponse.bodyToFlux(Book.class))
                .willReturn(Flux.just(book));

        // when & then
        StepVerifier.create(booksService.getBooksByAuthorAndTitle(authorName, titleName))
                .expectNext(book)
                .verifyComplete();

        verify(clientResponse).statusCode();
        verify(clientResponse).bodyToFlux(Book.class);
    }

    @Test
    void shouldReturnEmptyWhenAuthorAndTitleDoesNotExist() {
        // given
        String authorName = "Adam Mickiewicz";
        String titleName = "Dziady";
        ClientResponse clientResponse = mockWebClientResponse(
                webClient,
                "/books?authorName={authorName}&title={titleName}",
                authorName, titleName
        );
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.NOT_FOUND);

        // when & then
        StepVerifier.create(booksService.getBooksByAuthorAndTitle(authorName, titleName))
                .verifyComplete();

        verify(clientResponse).statusCode();
    }

    @Test
    void shouldThrowUnknownErrorExceptionWhenErrorWhileFindingBookByAuthorAndTitle() {
        // given
        String authorName = "Adam Mickiewicz";
        String titleName = "Dziady";
        ClientResponse clientResponse = mockWebClientResponse(
                webClient,
                "/books?authorName={authorName}&title={titleName}",
                authorName, titleName
        );
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.INTERNAL_SERVER_ERROR);

        // when & then
        StepVerifier.create(booksService.getBooksByAuthorAndTitle(authorName, titleName))
                .expectErrorMatches(throwable -> throwable instanceof DataServiceUnknownErrorException
                        && throwable.getMessage().equals("Oops! Something went wrong!"))
                .verify();

        verify(clientResponse).statusCode();
    }

    @Test
    void shouldReturnBookWhenBookFound() {
        // given
        ClientResponse clientResponse = mockWebClientResponse(
                webClient,
                "/books/{bookId}",
                1L
        );
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.OK);
        given(clientResponse.bodyToMono(Book.class))
                .willReturn(Mono.just(book));

        // when & then
        StepVerifier.create(booksService.getBookById(1L))
                .expectNext(book)
                .verifyComplete();

        verify(clientResponse).statusCode();
        verify(clientResponse).bodyToMono(Book.class);
    }

    @Test
    void shouldReturnEmptyWhenBookNotFound() {
        // given
        ClientResponse clientResponse = mockWebClientResponse(
                webClient,
                "/books/{bookId}",
                1L
        );
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.NOT_FOUND);

        // when & then
        StepVerifier.create(booksService.getBookById(1L))
                .verifyComplete();

        verify(clientResponse).statusCode();
    }

    @Test
    void shouldThrowUnknownErrorExceptionWhenErrorInService() {
        // given
        ClientResponse clientResponse = mockWebClientResponse(
                webClient,
                "/books/{bookId}",
                1L
        );
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.INTERNAL_SERVER_ERROR);

        // when & then
        StepVerifier.create(booksService.getBookById(1L))
                .expectErrorMatches(throwable -> throwable instanceof DataServiceUnknownErrorException
                        && throwable.getMessage().equals("Oops! Something went wrong!"))
                .verify();

        verify(clientResponse).statusCode();
    }

    public ClientResponse mockWebClientResponse(WebClient webClient,
                                                String uri,
                                                Object... uriVariables) {
        final var clientResponse = mock(ClientResponse.class);
        final var requestHeadersUriSpec = mock(WebClient.RequestHeadersUriSpec.class);

        when(webClient.get())
                .thenReturn(requestHeadersUriSpec);
        Optional.ofNullable(uriVariables)
                .ifPresentOrElse(variables -> {
                            final var requestHeadersSpec = mock(WebClient.RequestHeadersSpec.class);
                            when(requestHeadersUriSpec.uri(uri, variables))
                                    .thenReturn(requestHeadersSpec);
                            when(requestHeadersSpec.exchange())
                                    .thenReturn(Mono.just(clientResponse));
                        },
                        () -> when(requestHeadersUriSpec.exchange())
                                .thenReturn(Mono.just(clientResponse)));

        return clientResponse;
    }

}