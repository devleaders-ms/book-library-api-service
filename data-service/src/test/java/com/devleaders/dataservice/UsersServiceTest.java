package com.devleaders.dataservice;

import com.devleaders.dataservice.domain.User;
import com.devleaders.dataservice.exception.DataServiceUnknownErrorException;
import com.devleaders.model.BookRental;
import com.google.common.collect.Maps;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Map;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class UsersServiceTest {

    @Mock
    private WebClient webClient;

    @InjectMocks
    private UsersService usersService;

    private User user;
    private BookRental bookRental;
    private ClientResponse clientResponse;
    private Map<String, String> userData;


    @BeforeEach
    private void init() {
        MockitoAnnotations.initMocks(this);
        user = new User();
        bookRental = new BookRental();
        userData = Maps.newHashMap();
    }

    @Test
    void whenStatusOkThenUser() {
        mockUserClientResponse();
        mockWebClientForPost();
        when(clientResponse.statusCode()).thenReturn(HttpStatus.OK);

        StepVerifier.create(usersService.getUserByLogin("login"))
                .expectNext(user)
                .verifyComplete();
    }

    @Test
    void whenStatusNotFoundThenEmpty() {
        mockUserClientResponse();
        mockWebClientForPost();
        when(clientResponse.statusCode()).thenReturn(HttpStatus.NOT_FOUND);

        StepVerifier.create(usersService.getUserByLogin("login"))
                .verifyComplete();
    }

    @Test
    void whenStatusOtherThanOkOrNotFoundThenError() {
        mockUserClientResponse();
        mockWebClientForPost();
        when(clientResponse.statusCode()).thenReturn(HttpStatus.INTERNAL_SERVER_ERROR);

        StepVerifier.create(usersService.getUserByLogin("login"))
                .expectErrorMatches(throwable -> throwable instanceof Exception
                        && throwable.getMessage().equals("Oops! Something went wrong!"))
                .verify();
    }

    @Test
    void whenSaveThenUser() {
        mockUserClientResponse();
        var requestBodyUriSpec = mock(WebClient.RequestBodyUriSpec.class);
        var requestBodySpec = mock(WebClient.RequestBodySpec.class);
        var requestHeadersSpec = mock(WebClient.RequestHeadersSpec.class);

        when(webClient.post()).thenReturn(requestBodyUriSpec);
        when(requestBodyUriSpec.uri("/users")).thenReturn(requestBodySpec);
        when(requestBodySpec.bodyValue(user)).thenReturn(requestHeadersSpec);
        when(requestHeadersSpec.exchange()).thenReturn(Mono.just(clientResponse));

        when(clientResponse.statusCode()).thenReturn(HttpStatus.NOT_FOUND);

        StepVerifier.create(usersService.save(user))
                .expectNext(user)
                .verifyComplete();
    }

    @Test
    void shouldReturnUserWhenPatchSuccessful() {
        // given
        mockUserClientResponse();
        mockWebClientForPatch();
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.OK);

        // when & then
        StepVerifier.create(usersService.patch(userData, 1))
                .expectNext(user)
                .verifyComplete();

        verify(clientResponse).statusCode();
        verify(clientResponse).bodyToMono(User.class);
    }

    @Test
    void shouldReturnEmptyWhenUserNotFoundWhilePatch() {
        // given
        mockUserClientResponse();
        mockWebClientForPatch();
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.NOT_FOUND);

        // when & then
        StepVerifier.create(usersService.patch(userData, 1))
                .verifyComplete();

        verify(clientResponse).statusCode();
    }

    @Test
    void shouldThrowErrorWhenServiceErrorWhilePatch() {
        // given
        mockUserClientResponse();
        mockWebClientForPatch();
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.INTERNAL_SERVER_ERROR);

        // when & then
        StepVerifier.create(usersService.patch(userData, 1))
                .expectErrorMatches(throwable -> throwable instanceof DataServiceUnknownErrorException
                        && throwable.getMessage().equals("Oops! Something went wrong!"))
                .verify();

        verify(clientResponse).statusCode();
    }

    @Test
    void shouldThrowErrorWhenServiceErrorWhileGetUserById() {
        // given
        mockUserClientResponse();
        mockWebClientForGetUserById();
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.INTERNAL_SERVER_ERROR);

        // when & then
        StepVerifier.create(usersService.getUserById(1))
                .expectErrorMatches(throwable -> throwable instanceof DataServiceUnknownErrorException
                        && throwable.getMessage().equals("Oops! Something went wrong!"))
                .verify();

        verify(clientResponse).statusCode();
    }

    @Test
    void shouldReturnEmptyWhenUserNotFoundWhileGetUserById() {
        // given
        mockUserClientResponse();
        mockWebClientForGetUserById();
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.NOT_FOUND);

        // when & then
        StepVerifier.create(usersService.getUserById(1))
                .verifyComplete();

        verify(clientResponse).statusCode();
    }

    @Test
    void shouldReturnUserWhenGetUserByIdSuccessful() {
        // given
        mockUserClientResponse();
        mockWebClientForGetUserById();
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.OK);

        // when & then
        StepVerifier.create(usersService.getUserById(1L))
                .expectNext(user)
                .verifyComplete();

        verify(clientResponse).statusCode();
        verify(clientResponse).bodyToMono(User.class);
    }

    @Test
    void shouldThrowErrorWhenServiceErrorWhileGetBookRentalByUserId() {
        // given
        mockBookRentalClientResponse();
        mockWebClientForGetBookRental();
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.INTERNAL_SERVER_ERROR);

        // when & then
        StepVerifier.create(usersService.getBookRentalsByUserId(1))
                .expectErrorMatches(throwable -> throwable instanceof DataServiceUnknownErrorException
                        && throwable.getMessage().equals("Oops! Something went wrong!"))
                .verify();

        verify(clientResponse).statusCode();
    }

    @Test
    void shouldReturnEmptyWhenUserNotFoundWhileGetBookRentalByUserId() {
        // given
        mockBookRentalClientResponse();
        mockWebClientForGetBookRental();
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.NOT_FOUND);

        // when & then
        StepVerifier.create(usersService.getBookRentalsByUserId(1))
                .verifyComplete();

        verify(clientResponse).statusCode();
    }

    @Test
    void shouldReturnBookRentalWhenGetBookRentalByUserIdSuccessful() {
        // given
        mockBookRentalClientResponse();
        mockWebClientForGetBookRental();
        given(clientResponse.statusCode())
                .willReturn(HttpStatus.OK);

        // when & then
        StepVerifier.create(usersService.getBookRentalsByUserId(1L))
                .expectNext(bookRental)
                .verifyComplete();

        verify(clientResponse).statusCode();
        verify(clientResponse).bodyToFlux(BookRental.class);
    }

    private void mockUserClientResponse() {
        Mono<User> userMono = Mono.just(user);
        clientResponse = mock(ClientResponse.class);
        when(clientResponse.bodyToMono(User.class)).thenReturn(userMono);
    }

    private void mockBookRentalClientResponse() {
        Flux<BookRental> bookRentalFlux = Flux.just(bookRental);
        clientResponse = mock(ClientResponse.class);
        when(clientResponse.bodyToFlux(BookRental.class)).thenReturn(bookRentalFlux);
    }

    private void mockWebClientForPost() {
        final var requestHeadersUriSpec = mock(WebClient.RequestHeadersUriSpec.class);
        final var requestHeadersSpec = mock(WebClient.RequestHeadersSpec.class);

        when(webClient.get()).thenReturn(requestHeadersUriSpec);
        when(requestHeadersUriSpec.uri("/users?login={login}", "login")).thenReturn(requestHeadersSpec);
        when(requestHeadersSpec.exchange()).thenReturn(Mono.just(clientResponse));
    }

    private void mockWebClientForPatch() {
        final var requestBodyUriSpec = mock(WebClient.RequestBodyUriSpec.class);
        final var requestBodySpec = mock(WebClient.RequestBodySpec.class);
        final var requestHeadersSpec = mock(WebClient.RequestHeadersSpec.class);

        given(webClient.patch()).willReturn(requestBodyUriSpec);
        given(requestBodyUriSpec.uri("/users/{userId}", 1)).willReturn(requestBodySpec);
        given(requestBodySpec.bodyValue(userData)).willReturn(requestHeadersSpec);
        given(requestHeadersSpec.exchange()).willReturn(Mono.just(clientResponse));
    }

    private void mockWebClientForGetUserById() {
        final var requestHeadersUriSpec = mock(WebClient.RequestHeadersUriSpec.class);
        final var requestHeadersSpec = mock(WebClient.RequestHeadersSpec.class);

        given(webClient.get()).willReturn(requestHeadersUriSpec);
        given(requestHeadersUriSpec.uri("/users/{id}", 1L)).willReturn(requestHeadersSpec);
        given(requestHeadersSpec.exchange()).willReturn(Mono.just(clientResponse));
    }

    private void mockWebClientForGetBookRental() {
        final var requestHeadersUriSpec = mock(WebClient.RequestHeadersUriSpec.class);
        final var requestHeadersSpec = mock(WebClient.RequestHeadersSpec.class);

        given(webClient.get()).willReturn(requestHeadersUriSpec);
        given(requestHeadersUriSpec.uri("/bookRentals?userId={userId}", 1L)).willReturn(requestHeadersSpec);
        given(requestHeadersSpec.exchange()).willReturn(Mono.just(clientResponse));
    }
}