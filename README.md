# README #

### What is this repository for? ###

* book library api service

### How do I get set up? ###

* To run do mvn clean install in book-library-api-service then spring-boot run in distribution

* Dependencies : SWAGGER, LOMBOK, REFACTOR, WEBFLUX

* How to run tests : Mavern build

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact