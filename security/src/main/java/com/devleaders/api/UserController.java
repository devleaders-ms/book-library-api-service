package com.devleaders.api;

import com.devleaders.api.exception.UnknownErrorException;
import com.devleaders.api.exception.UserNotFoundException;
import com.devleaders.api.mapper.UserMapper;
import com.devleaders.dataservice.UsersService;
import com.devleaders.model.UserData;
import com.devleaders.model.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class UserController implements UsersRestApi {

    private final UserMapper userMapper;
    private final UsersService usersService;
    private final PasswordEncoder passwordEncoder;

    @Override
    public Flux<UserDto> getUsers(@Valid String fullName) {
        return usersService.getUsersByFullNameLike(fullName)
                .map(userMapper::mapUserToUserDto);
    }

    @Override
    public Mono<Void> patchUser(@Min(1) Integer id, @Valid Mono<UserData> userData) {
        return userData
                .map(this::mapUserData)
                .flatMap(map -> usersService.patch(map, id))
                .onErrorMap(throwable -> new UnknownErrorException())
                .switchIfEmpty(Mono.error(new UserNotFoundException()))
                .then();
    }

    private Map<String, String> mapUserData(UserData userData) {
        HashMap<String, String> map = new HashMap<>();
        if (userData.getEmail() != null) {
            map.put("email", userData.getEmail());
        }
        if (userData.getPassword() != null) {
            String encodedPassword = passwordEncoder.encode(userData.getPassword());
            map.put("password", encodedPassword);
        }
        return map;
    }
}
