package com.devleaders.api.config;

import io.jsonwebtoken.ExpiredJwtException;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMessage;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class SecurityContextRepository implements ServerSecurityContextRepository {

    private static final int BEARER_PLACEHOLDER = 7;
    private final AuthenticationManager authenticationManager;

    @Override
    public Mono<Void> save(ServerWebExchange serverWebExchange, SecurityContext securityContext) {
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public Mono<SecurityContext> load(ServerWebExchange serverWebExchange) {
        return Mono.just(serverWebExchange)
                .map(ServerWebExchange::getRequest)
                .map(HttpMessage::getHeaders)
                .filter(headers -> headers.containsKey(HttpHeaders.AUTHORIZATION))
                .map(headers -> headers.getFirst(HttpHeaders.AUTHORIZATION))
                .filter(header -> header.startsWith("Bearer"))
                .map(header -> header.substring(BEARER_PLACEHOLDER))
                .map(token -> new UsernamePasswordAuthenticationToken(null, token))
                .flatMap(authenticationManager::authenticate)
                .<SecurityContext>map(SecurityContextImpl::new)
                .onErrorMap(
                        error -> error instanceof ExpiredJwtException,
                        autEx -> new ResponseStatusException(HttpStatus.UNAUTHORIZED, "JWT token expired")
                );
    }
}
