package com.devleaders.api.config;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.ReactiveAuthorizationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.server.authorization.AuthorizationContext;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class ApiAuthorizationManager implements ReactiveAuthorizationManager<AuthorizationContext> {

    private static final String USER_ID = "userId";

    private final JwtUtil jwtUtil;

    @Override
    public Mono<AuthorizationDecision> check(Mono<Authentication> authenticationMono,
                                             AuthorizationContext authorizationContext) {
        Mono<Long> userIdFromPath = getUserIdFromAuthorizationContext(authorizationContext);
        Mono<Long> userIdFromAuthentication = getUserIdFromAuthentication(authenticationMono);
        return Mono.zip(userIdFromAuthentication, userIdFromPath)
                .filter(tuple -> tuple.getT1().equals(tuple.getT2()))
                .map(__ -> new AuthorizationDecision(true))
                .switchIfEmpty(Mono.just(new AuthorizationDecision(false)));

    }

    private Mono<Long> getUserIdFromAuthentication(Mono<Authentication> authenticationMono) {
        return authenticationMono
                .map(authentication -> (String) authentication.getCredentials())
                .flatMap(jwtUtil::getAllClaimsFromToken)
                .filter(claims -> claims.containsKey(USER_ID))
                .map(claims -> (Integer) claims.get(USER_ID))
                .map(String::valueOf)
                .map(Long::valueOf);
    }

    private Mono<Long> getUserIdFromAuthorizationContext(AuthorizationContext authorizationContext) {
        return Mono.just(authorizationContext)
                .map(AuthorizationContext::getVariables)
                .filter(variables -> variables.containsKey(USER_ID))
                .map(variables -> (String) variables.get(USER_ID))
                .map(Long::valueOf);
    }

}
