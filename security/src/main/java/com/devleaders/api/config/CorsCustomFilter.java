package com.devleaders.api.config;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.CorsProcessor;
import org.springframework.web.cors.reactive.CorsWebFilter;

@Order(Ordered.HIGHEST_PRECEDENCE)
public class CorsCustomFilter extends CorsWebFilter {
    public CorsCustomFilter(CorsConfigurationSource configSource) {
        super(configSource);
    }

    public CorsCustomFilter(CorsConfigurationSource configSource, CorsProcessor processor) {
        super(configSource, processor);
    }
}
