package com.devleaders.api.config;

public enum TokenType {
    REFRESH_TOKEN,
    ACCESS_TOKEN
}
