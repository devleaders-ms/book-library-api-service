package com.devleaders.api.config;

import com.devleaders.api.exception.InvalidJwtTokenException;
import com.devleaders.dataservice.domain.Role;
import com.devleaders.dataservice.domain.RoleType;
import com.devleaders.dataservice.domain.User;
import com.devleaders.model.JwtResponse;
import com.google.common.annotations.VisibleForTesting;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.io.DeserializationException;
import io.jsonwebtoken.security.Keys;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseCookie;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@NoArgsConstructor
public class JwtUtil {

    @Value("${security-module.jwt.secretKey}")
    private String secretKey;
    private final String tokenFormatType = "bearer";
    @Value("${security-module.jwt.accessTokenExpirationTimeSec}")
    private long accessTokenExpirationTimeSec;
    @Value("${security-module.jwt.refreshTokenExpirationTimeSec}")
    private long refreshTokenExpirationTimeSec;

    @VisibleForTesting
    public JwtUtil(String secretKey, long accessTokenExpirationTimeSec, long refreshTokenExpirationTimeSec) {
        this.secretKey = secretKey;
        this.accessTokenExpirationTimeSec = accessTokenExpirationTimeSec;
        this.refreshTokenExpirationTimeSec = refreshTokenExpirationTimeSec;
    }

    public Mono<Claims> getAllClaimsFromToken(String token) {

        return Mono.defer(
                () -> Mono.just(
                        Jwts.parserBuilder()
                                .setSigningKey(secretKey.getBytes())
                                .build()
                                .parseClaimsJws(token)
                                .getBody()
                )
        )
                .onErrorMap(MalformedJwtException.class, err -> new InvalidJwtTokenException())
                .onErrorMap(DeserializationException.class, err -> new InvalidJwtTokenException());
    }

    public Mono<JwtResponse> generateTokens(User user, ServerWebExchange serverWebExchange) {
        Mono<String> accessToken = generateToken(user, TokenType.ACCESS_TOKEN);
        Mono<String> refreshToken = generateToken(user, TokenType.REFRESH_TOKEN);

        return refreshToken
                .flatMap(token -> {
                    serverWebExchange.getResponse().addCookie(
                            ResponseCookie.from("refresh_token", token)
                                    .maxAge(refreshTokenExpirationTimeSec)
                                    .httpOnly(true)
                                    .sameSite("lax")
                                    .path("/v1/api/auth/token")
                                    .build()
                    );
                    return accessToken;
                })
                .map(token -> JwtResponse.builder()
                        .accessToken(token)
                        .expiresIn(accessTokenExpirationTimeSec)
                        .tokenType("bearer")
                        .build()
                );
    }

    public Mono<JwtResponse> exchangeRefreshToken(User user) {
        return generateToken(user, TokenType.ACCESS_TOKEN)
                .map(accessToken -> JwtResponse.builder()
                        .accessToken(accessToken)
                        .tokenType(tokenFormatType)
                        .expiresIn(accessTokenExpirationTimeSec)
                        .build()
                );
    }

    private Mono<String> generateToken(User user, TokenType tokenType) {
        List<RoleType> roles = user.getRoles()
                .stream()
                .map(Role::getRoleType)
                .collect(Collectors.toList());
        Map<String, Object> claims = new HashMap<>();
        claims.put("roles", roles);
        claims.put("userId", user.getId());
        claims.put("firstName", user.getFirstName());
        claims.put("lastName", user.getLastName());
        claims.put("email", user.getEmail());
        claims.put("token_type", tokenType.name());

        long expirationTime = tokenType == TokenType.REFRESH_TOKEN
                ? refreshTokenExpirationTimeSec
                : accessTokenExpirationTimeSec;
        LocalDateTime localDateTime = LocalDateTime.now().plusSeconds(expirationTime);
        final Date expiration = Timestamp.valueOf(localDateTime);

        return Mono.just(
                Jwts.builder()
                        .setClaims(claims)
                        .setSubject(user.getLogin())
                        .setExpiration(expiration)
                        .signWith(Keys.hmacShaKeyFor(secretKey.getBytes()))
                        .compact()
        );
    }

}
