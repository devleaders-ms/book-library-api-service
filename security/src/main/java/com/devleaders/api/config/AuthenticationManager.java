package com.devleaders.api.config;

import lombok.AllArgsConstructor;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

import static com.devleaders.api.config.TokenType.ACCESS_TOKEN;

@Component
@AllArgsConstructor
public class AuthenticationManager implements ReactiveAuthenticationManager {

    private final JwtUtil jwtUtil;

    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        return Mono.just(authentication)
                .map(auth -> auth.getCredentials().toString())
                .flatMap(this::getAuthenticationFromToken);
    }

    @SuppressWarnings("unchecked")
    private Mono<Authentication> getAuthenticationFromToken(String token) {
        return jwtUtil.getAllClaimsFromToken(token)
                .filter(claims -> claims.getOrDefault("token_type", "").equals(ACCESS_TOKEN.name()))
                .map(claims -> {
                    String username = claims.getSubject();
                    List<SimpleGrantedAuthority> roles = ((List<String>) claims.get("roles"))
                            .stream()
                            .map(SimpleGrantedAuthority::new)
                            .collect(Collectors.toList());
                    return new UsernamePasswordAuthenticationToken(
                            username,
                            token,
                            roles
                    );
                });
    }

}