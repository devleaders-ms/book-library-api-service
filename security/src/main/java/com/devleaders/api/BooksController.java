package com.devleaders.api;

import com.devleaders.api.exception.BookNotFoundException;
import com.devleaders.api.exception.UnknownErrorException;
import com.devleaders.dataservice.BooksService;
import com.devleaders.model.Book;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequiredArgsConstructor
public class BooksController implements BooksRestApi {

    private final BooksService booksService;

    @Override
    public Mono<Book> addBook(@Valid Mono<Book> book) {
        return book.flatMap(booksService::createBook);
    }

    @Override
    public Mono<Book> getBook(@Min(1) Integer bookId) {
        return booksService.getBookById(bookId)
                .onErrorMap(throwable -> new UnknownErrorException())
                .switchIfEmpty(Mono.error(new BookNotFoundException()));
    }

    @Override
    public Flux<Book> getBooks(@Valid String titleName, @Valid String authorName) {
        if (titleName != null && authorName != null) {
            return booksService.getBooksByAuthorAndTitle(authorName, titleName);
        } else if (titleName != null) {
            return booksService.getBooksByTitleLike(titleName);
        } else if (authorName != null) {
            return booksService.getBooksByAuthor(authorName);
        }
        return booksService.getAllBooks();
    }
}
