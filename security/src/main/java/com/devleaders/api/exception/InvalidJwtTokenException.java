package com.devleaders.api.exception;

public class InvalidJwtTokenException extends RuntimeException {
    public InvalidJwtTokenException() {
        super("Provided token is incorrect");
    }
}
