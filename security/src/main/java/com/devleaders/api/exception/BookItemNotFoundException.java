package com.devleaders.api.exception;

class BookItemNotFoundException extends RuntimeException {
    BookItemNotFoundException() {
        super("BookItem does not exist exception");
    }
}