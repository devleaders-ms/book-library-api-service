package com.devleaders.api.exception;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@EqualsAndHashCode
public class Error {
    private Map<String, String> errorPayload;

    private Error(Map<String, String> errorPayload) {
        this.errorPayload = errorPayload;
    }

    public static Error createErrorFromMessage(String message) {
        HashMap<String, String> error = new HashMap<>();
        error.put("message", message);
        return new Error(error);
    }
}
