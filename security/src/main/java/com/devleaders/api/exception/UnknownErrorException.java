package com.devleaders.api.exception;

public class UnknownErrorException extends RuntimeException {
    public UnknownErrorException() {
        super("Oops! Something went wrong!");
    }
}
