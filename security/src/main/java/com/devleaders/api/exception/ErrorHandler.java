package com.devleaders.api.exception;

import com.devleaders.dataservice.exception.BookAlreadyExistsException;
import com.devleaders.dataservice.exception.BookAlreadyRentException;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import reactor.core.publisher.Mono;

@RestControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(BookAlreadyRentException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public Mono<Error> bookAlreadyRent(BookAlreadyRentException exception) {
        return toError(exception.getMessage());
    }

    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Mono<Error> userNotFound(UserNotFoundException exception) {
        return toError(exception.getMessage());
    }

    @ExceptionHandler(AuthenticationFailedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Mono<Error> incorrectPassword(AuthenticationFailedException exception) {
        return toError(exception.getMessage());
    }

    @ExceptionHandler(UnknownErrorException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Mono<Error> internalServerError(UnknownErrorException exception) {
        return toError(exception.getMessage());
    }

    @ExceptionHandler(UsernameAlreadyTakenException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public Mono<Error> userAlreadyTaken(UsernameAlreadyTakenException exception) {
        return toError(exception.getMessage());
    }

    @ExceptionHandler(BookNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Mono<Error> bookNotFound(BookNotFoundException exception) {
        return toError(exception.getMessage());
    }

    @ExceptionHandler(BookItemNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Mono<Error> bookItemNotFound(BookItemNotFoundException exception) {
        return toError(exception.getMessage());
    }

    @ExceptionHandler(BookAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public Mono<Error> handle(BookAlreadyExistsException exception) {
        return toError(exception.getMessage());
    }

    @ExceptionHandler(InvalidJwtTokenException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Mono<Error> handle(InvalidJwtTokenException exception) {
        return toError(exception.getMessage());
    }

    @ExceptionHandler(ExpiredJwtException.class)
    public Mono<Error> handle(ExpiredJwtException exception) {
        return toError("Jwt token expired");
    }

    private Mono<Error> toError(String message) {
        return Mono.just(Error.createErrorFromMessage(message));
    }
}
