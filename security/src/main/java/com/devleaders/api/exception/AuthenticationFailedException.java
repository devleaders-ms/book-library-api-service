package com.devleaders.api.exception;

public class AuthenticationFailedException extends RuntimeException {
    public AuthenticationFailedException() {
        super("Authorization failed");
    }
}
