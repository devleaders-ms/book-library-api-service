package com.devleaders.api;

import com.devleaders.api.config.JwtUtil;
import com.devleaders.api.exception.AuthenticationFailedException;
import com.devleaders.api.exception.InvalidJwtTokenException;
import com.devleaders.api.exception.UnknownErrorException;
import com.devleaders.api.exception.UsernameAlreadyTakenException;
import com.devleaders.api.mapper.UserMapper;
import com.devleaders.dataservice.UsersService;
import com.devleaders.dataservice.domain.User;
import com.devleaders.model.JwtResponse;
import com.devleaders.model.LoginRequest;
import com.devleaders.model.RegisterRequest;
import com.devleaders.model.UserDto;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpCookie;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import javax.validation.Valid;
import java.util.Optional;

import static com.devleaders.api.config.TokenType.REFRESH_TOKEN;

@RestController
@RequiredArgsConstructor
public class AuthController implements AuthRestApi {

    private final UsersService usersService;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;
    private final JwtUtil jwtUtil;

    @Override
    public Mono<JwtResponse> authenticateUser(@Valid Mono<LoginRequest> request,
                                              ServerWebExchange serverWebExchange) {
        Mono<LoginRequest> loginRequest = request.cache();
        Mono<User> userMono = loginRequest.map(LoginRequest::getLogin)
                .flatMap(usersService::getUserByLogin)
                .onErrorMap(throwable -> new UnknownErrorException())
                .switchIfEmpty(Mono.error(new AuthenticationFailedException()));
        Mono<String> requestPassword = loginRequest.map(LoginRequest::getPassword);

        return Mono.zip(userMono, requestPassword)
                .filter(tuple -> isPasswordMatching(tuple.getT1(), tuple.getT2()))
                .switchIfEmpty(Mono.error(new AuthenticationFailedException()))
                .map(Tuple2::getT1)
                .flatMap(user -> jwtUtil.generateTokens(user, serverWebExchange));
    }

    @Override
    public Mono<JwtResponse> exchangeToken(ServerWebExchange serverWebExchange) {
        Mono<String> refreshToken = Optional.ofNullable(serverWebExchange)
                .map(ServerWebExchange::getRequest)
                .map(ServerHttpRequest::getCookies)
                .map(cookies -> cookies.getFirst("refresh_token"))
                .map(HttpCookie::getValue)
                .map(Mono::just)
                .orElseGet(() -> Mono.error(new InvalidJwtTokenException()))
                .cache();
        Mono<Claims> tokenClaims = refreshToken.flatMap(jwtUtil::getAllClaimsFromToken).cache();
        Mono<String> tokenType = tokenClaims.map(claims -> claims.get("token_type", String.class));
        Mono<User> user = tokenClaims.map(claims -> claims.get("userId", Integer.class))
                .flatMap(usersService::getUserById);

        return tokenType.filter(type -> type.equals(REFRESH_TOKEN.name()))
                .switchIfEmpty(Mono.error(new InvalidJwtTokenException()))
                .then(user)
                .flatMap(jwtUtil::exchangeRefreshToken);
    }

    @Override
    public Mono<UserDto> registerUser(@Valid Mono<RegisterRequest> registerRequest) {
        Mono<RegisterRequest> requestMono = registerRequest.cache();
        return requestMono.map(RegisterRequest::getLogin)
                .flatMap(usersService::getUserByLogin)
                .hasElement()
                .flatMap(exists -> {
                    if (exists) {
                        return Mono.error(new UsernameAlreadyTakenException());
                    } else {
                        return requestMono.flatMap(this::saveUser);
                    }
                })
                .map(userMapper::mapUserToUserDto);
    }

    private boolean isPasswordMatching(User user, String requestPassword) {
        return passwordEncoder.matches(requestPassword, user.getPassword());
    }

    private Mono<User> saveUser(RegisterRequest registerRequest) {
        User user = userMapper.mapRegisterRequestToUser(registerRequest, passwordEncoder.encode(registerRequest.getPassword()));
        return usersService.save(user);
    }

}