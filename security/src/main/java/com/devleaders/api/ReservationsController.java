package com.devleaders.api;

import com.devleaders.api.exception.UnknownErrorException;
import com.devleaders.api.exception.UserNotFoundException;
import com.devleaders.dataservice.ReservationsService;
import com.devleaders.dataservice.UsersService;
import com.devleaders.dataservice.exception.DataServiceUnknownErrorException;
import com.devleaders.dataservice.exception.DataServiceUserNotFoundException;
import com.devleaders.model.Reservation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import javax.validation.constraints.Min;

@RestController
@RequiredArgsConstructor
public class ReservationsController implements ReservationsRestApi {

    private final UsersService usersService;
    private final ReservationsService reservationsService;

    @Override
    public Flux<Reservation> getReservations() {
        return reservationsService
                .getReservations()
                .onErrorMap(throwable -> new UnknownErrorException());
    }

    @Override
    public Flux<Reservation> getReservationsByUserId(@Min(1) Integer id) {
        return reservationsService.getReservationsByUserId(id)
                .onErrorMap(DataServiceUnknownErrorException.class, throwable -> new UnknownErrorException())
                .onErrorMap(DataServiceUserNotFoundException.class, throwable -> new UserNotFoundException());
    }
}
