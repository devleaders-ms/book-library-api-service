package com.devleaders.api;

import com.devleaders.api.exception.UnknownErrorException;
import com.devleaders.api.exception.UserNotFoundException;
import com.devleaders.dataservice.BookItemService;
import com.devleaders.dataservice.RentalsService;
import com.devleaders.dataservice.UsersService;
import com.devleaders.dataservice.exception.DataServiceUnknownErrorException;
import com.devleaders.model.BookRental;
import com.devleaders.model.CreateRentalRequest;
import com.devleaders.model.RentAction;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequiredArgsConstructor
public class RentalsController implements RentalsRestApi {

    private final UsersService usersService;
    private final RentalsService rentalsService;
    private final BookItemService bookItemService;

    @Override
    public Mono<BookRental> getBookRental(@Valid Mono<CreateRentalRequest> createRentalRequest) {
        return createRentalRequest.flatMap(rentalRequest -> {
            if (rentalRequest.getType() == RentAction.BORROW) {
                return bookItemService.changeBookItemAvailability(rentalRequest.getBookItemId())
                        .flatMap(bookItem -> rentalsService.rentBook(rentalRequest.getBookItemId(), rentalRequest.getUserId()));
            }
            return Mono.error(new RuntimeException("Not implemented yet !"));
        });
    }

    @Override
    public Flux<BookRental> getRentalsByUserId(@Min(1) Integer id) {
        return usersService
                .getUserById(id)
                .onErrorMap(throwable -> new UnknownErrorException())
                .switchIfEmpty(Mono.error(new UserNotFoundException()))
                .flatMapMany(__ -> usersService.getBookRentalsByUserId(id))
                .onErrorMap(DataServiceUnknownErrorException.class, throwable -> new UnknownErrorException());
    }

}