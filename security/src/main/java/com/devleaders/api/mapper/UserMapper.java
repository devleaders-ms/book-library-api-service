package com.devleaders.api.mapper;

import com.devleaders.dataservice.domain.User;
import com.devleaders.model.RegisterRequest;
import com.devleaders.model.UserDto;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class UserMapper {

    public User mapRegisterRequestToUser(RegisterRequest registerRequest, String encodedPassword) {
        return User.builder()
                .login(registerRequest.getLogin())
                .password(encodedPassword)
                .email(registerRequest.getEmail())
                .firstName(registerRequest.getFirstName())
                .lastName(registerRequest.getLastName())
                .build();
    }

    public UserDto mapUserToUserDto(User user) {
        return UserDto.builder()
                .id(user.getId())
                .login(user.getLogin())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .build();
    }
}
