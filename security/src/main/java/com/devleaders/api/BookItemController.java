package com.devleaders.api;

import com.devleaders.api.exception.BookNotFoundException;
import com.devleaders.api.exception.UnknownErrorException;
import com.devleaders.dataservice.BookItemService;
import com.devleaders.dataservice.BooksService;
import com.devleaders.dataservice.exception.DataServiceBookNotFoundException;
import com.devleaders.dataservice.exception.DataServiceUnknownErrorException;
import com.devleaders.model.Book;
import com.devleaders.model.BookItem;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@RestController
@RequiredArgsConstructor
public class BookItemController implements BookItemsRestApi {

    private final BooksService booksService;
    private final BookItemService bookItemService;

    @Override
    public Flux<BookItem> getBookItems(@NotNull @Min(1) @Valid Integer bookId) {
        return booksService
                .getBookById(bookId)
                .onErrorMap(throwable -> new UnknownErrorException())
                .switchIfEmpty(Mono.error(new BookNotFoundException()))
                .map(Book::getId)
                .flatMapMany(bookItemService::getBookItemsForBookId);
    }

    @Override
    public Flux<BookItem> getAvailableBookItems(@Min(1) Integer bookId) {
        return bookItemService.getAvailableBookItems(bookId)
                .onErrorMap(DataServiceBookNotFoundException.class, throwable -> new BookNotFoundException())
                .onErrorMap(DataServiceUnknownErrorException.class, throwable -> new UnknownErrorException());
    }
}
