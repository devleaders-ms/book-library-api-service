package com.devleaders.api;

import com.devleaders.api.exception.UnknownErrorException;
import com.devleaders.api.exception.UserNotFoundException;
import com.devleaders.dataservice.UsersService;
import com.devleaders.dataservice.domain.User;
import com.devleaders.model.UserData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.BDDMockito.given;

class UserControllerTest {

    @Mock
    private UsersService usersService;
    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserController userController;

    private User user;
    private Mono<UserData> userDataMono;

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
        user = new User();
        UserData userData = new UserData();
        userData.setEmail("email@mail.com");
        userData.setPassword("password");
        userDataMono = Mono.just(userData);
        given(passwordEncoder.encode("password")).willReturn("encodedPassword");
    }

    @Test
    void shouldReturnEmptyWhenSuccess() {
        // given
        given(usersService.patch(anyMap(), anyInt()))
                .willReturn(Mono.just(user));

        // when & then
        StepVerifier.create(userController.patchUser(1, userDataMono))
                .verifyComplete();
    }

    @Test
    void shouldThrowUserNotFoundExceptionWhenNoUserFound() {
        // given
        given(usersService.patch(anyMap(), anyInt()))
                .willReturn(Mono.empty());

        // when & then
        StepVerifier.create(userController.patchUser(1, userDataMono))
                .expectErrorMatches(throwable -> throwable instanceof UserNotFoundException
                        && throwable.getMessage().equals("User not found"))
                .verify();
    }

    @Test
    void shouldThrowUnknownErrorExceptionWhenServiceError() {
        // given
        given(usersService.patch(anyMap(), anyInt()))
                .willReturn(Mono.error(new UnknownErrorException()));

        // when & then
        StepVerifier.create(userController.patchUser(1, userDataMono))
                .expectErrorMatches(throwable -> throwable instanceof UnknownErrorException
                        && throwable.getMessage().equals("Oops! Something went wrong!"))
                .verify();
    }
}