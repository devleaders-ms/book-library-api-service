package com.devleaders.api.exception;

import io.jsonwebtoken.ExpiredJwtException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

class ErrorHandlerTest {

    private ErrorHandler errorHandler;

    @BeforeEach
    void init() {
        errorHandler = new ErrorHandler();
    }

    @Test
    void whenUserNotFoundExceptionThenErrorMessage() {
        UserNotFoundException exception = new UserNotFoundException();
        Error errorFromMessage = Error.createErrorFromMessage(exception.getMessage());

        Mono<Error> errorMono = errorHandler.userNotFound(exception);

        StepVerifier.create(errorMono)
                .expectNextMatches(error -> error.equals(errorFromMessage))
                .verifyComplete();
    }

    @Test
    void whenAuthenticationFailedExceptionThenErrorMessage() {
        AuthenticationFailedException exception = new AuthenticationFailedException();
        Error errorFromMessage = Error.createErrorFromMessage(exception.getMessage());

        Mono<Error> errorMono = errorHandler.incorrectPassword(exception);

        StepVerifier.create(errorMono)
                .expectNextMatches(error -> error.equals(errorFromMessage))
                .verifyComplete();
    }

    @Test
    void whenUnknownErrorExceptionThenErrorMessage() {
        UnknownErrorException exception = new UnknownErrorException();

        Mono<Error> errorMono = errorHandler.internalServerError(exception);

        StepVerifier.create(errorMono)
                .expectNextMatches(error -> error.getErrorPayload().get("message").equals(exception.getMessage()))
                .verifyComplete();
    }

    @Test
    void whenUserAlreadyTakenExceptionThenErrorMessage() {
        UsernameAlreadyTakenException exception = new UsernameAlreadyTakenException();

        Mono<Error> errorMono = errorHandler.userAlreadyTaken(exception);

        StepVerifier.create(errorMono)
                .expectNextMatches(error -> error.getErrorPayload().get("message").equals(exception.getMessage()))
                .verifyComplete();
    }

    @Test
    void whenBookNotFoundThenErrorMessage() {
        BookNotFoundException exception = new BookNotFoundException();

        Mono<Error> errorMono = errorHandler.bookNotFound(exception);

        StepVerifier.create(errorMono)
                .expectNextMatches(error -> error.getErrorPayload().get("message").equals(exception.getMessage()))
                .verifyComplete();
    }

    @Test
    void whenBookItemNotFoundThenErrorMessage() {
        // given
        BookItemNotFoundException exception = new BookItemNotFoundException();

        // when
        Mono<Error> errorMono = errorHandler.bookItemNotFound(exception);

        // then
        StepVerifier.create(errorMono)
                .expectNextMatches(error -> error.getErrorPayload().get("message").equals(exception.getMessage()))
                .verifyComplete();
    }

    @Test
    void shouldHandleInvalidJwtTokenException() {
        // given
        InvalidJwtTokenException exception = new InvalidJwtTokenException();

        // when
        Mono<Error> errorMono = errorHandler.handle(exception);

        // then
        StepVerifier.create(errorMono)
                .expectNextMatches(error -> error.getErrorPayload().get("message").equals(exception.getMessage()))
                .verifyComplete();
    }

    @Test
    void shouldHandleExpiredJwtException() {
        // given
        ExpiredJwtException exception = new ExpiredJwtException(null, null, null);

        // when
        Mono<Error> errorMono = errorHandler.handle(exception);

        // then
        StepVerifier.create(errorMono)
                .expectNextMatches(error -> error.getErrorPayload().get("message").equals("Jwt token expired"))
                .verifyComplete();
    }

}