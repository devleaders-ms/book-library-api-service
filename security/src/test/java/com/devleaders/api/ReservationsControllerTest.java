package com.devleaders.api;

import com.devleaders.api.exception.UnknownErrorException;
import com.devleaders.api.exception.UserNotFoundException;
import com.devleaders.dataservice.ReservationsService;
import com.devleaders.dataservice.UsersService;
import com.devleaders.dataservice.domain.User;
import com.devleaders.dataservice.exception.DataServiceUserNotFoundException;
import com.devleaders.model.Reservation;
import com.devleaders.model.ReservationStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDate;

import static org.mockito.BDDMockito.given;

class ReservationsControllerTest {

    @Mock
    private ReservationsService reservationsService;
    @Mock
    private UsersService usersService;

    @InjectMocks
    private ReservationsController reservationsController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldReturnReservationsWhenExists() {
        // given
        Reservation reservation = new Reservation(1L, 1L, 1L, LocalDate.now(), ReservationStatus.AWAITING);
        given(reservationsService.getReservations())
                .willReturn(Flux.just(reservation));

        // when && then
        Flux<Reservation> reservations = reservationsController.getReservations();
        StepVerifier.create(reservations)
                .expectNext(reservation)
                .verifyComplete();
    }

    @Test
    void shouldReturnReservationsForUserWhenExists() {
        // given
        Reservation reservation = new Reservation(1L, 1L, 1L, LocalDate.now(), ReservationStatus.AWAITING);
        User user = new User();
        user.setId(1);
        given(usersService.getUserById(1))
                .willReturn(Mono.just(user));
        given(reservationsService.getReservationsByUserId(1))
                .willReturn(Flux.just(reservation));

        // when && then
        Flux<Reservation> reservations = reservationsController.getReservationsByUserId(1);
        StepVerifier.create(reservations)
                .expectNext(reservation)
                .verifyComplete();
    }

    @Test
    void shouldThrowUserNotFoundExceptionWhenUserNotExists() {
        // given
        Reservation reservation = new Reservation(1L, 1L, 1L, LocalDate.now(), ReservationStatus.AWAITING);
        given(reservationsService.getReservationsByUserId(1))
                .willReturn(Flux.error(new DataServiceUserNotFoundException()));

        // when && then
        Flux<Reservation> reservations = reservationsController.getReservationsByUserId(1);
        StepVerifier.create(reservations)
                .expectError(UserNotFoundException.class)
                .verify();
    }

    @Test
    void shouldReturnEmptyWhenNoReservations() {
        // given
        given(reservationsService.getReservations())
                .willReturn(Flux.empty());

        // when && then
        Flux<Reservation> reservations = reservationsController.getReservations();
        StepVerifier.create(reservations)
                .verifyComplete();
    }

    @Test
    void shouldThrowUnknownErrorExceptionWhenInternalServerError() {
        // given
        given(reservationsService.getReservations())
                .willReturn(Flux.error(new UnknownErrorException()));

        // when && then
        Flux<Reservation> reservations = reservationsController.getReservations();
        StepVerifier.create(reservations)
                .expectErrorMatches(throwable -> throwable instanceof UnknownErrorException
                        && throwable.getMessage().equals("Oops! Something went wrong!"))
                .verify();
    }
}