package com.devleaders.api;

import com.devleaders.api.config.JwtUtil;
import com.devleaders.api.exception.AuthenticationFailedException;
import com.devleaders.api.exception.InvalidJwtTokenException;
import com.devleaders.api.exception.UnknownErrorException;
import com.devleaders.api.exception.UsernameAlreadyTakenException;
import com.devleaders.api.mapper.UserMapper;
import com.devleaders.dataservice.UsersService;
import com.devleaders.dataservice.domain.User;
import com.devleaders.model.JwtResponse;
import com.devleaders.model.LoginRequest;
import com.devleaders.model.RegisterRequest;
import com.devleaders.model.UserDto;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpCookie;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.devleaders.api.config.TokenType.ACCESS_TOKEN;
import static com.devleaders.api.config.TokenType.REFRESH_TOKEN;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class AuthControllerTest {

    @Mock
    private UsersService usersService;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private UserMapper userMapper;
    @Mock
    private JwtUtil jwtUtil;

    @InjectMocks
    private AuthController authController;

    @BeforeEach
    private void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldThrowUserNotFoundExceptionWhenUserNotExists() {
        // given
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);
        Mono<LoginRequest> loginRequest = Mono.just(new LoginRequest("login", "password"));
        given(usersService.getUserByLogin("login"))
                .willReturn(Mono.empty());

        // when & then
        StepVerifier.create(authController.authenticateUser(loginRequest, serverWebExchange))
                .expectError(AuthenticationFailedException.class)
                .verify();

        verify(usersService).getUserByLogin("login");
    }

    @Test
    void shouldThrowUnknownErrorExceptionWhenErrorInUsersService() {
        // given
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);
        Mono<LoginRequest> loginRequest = Mono.just(new LoginRequest("login", "password"));
        given(usersService.getUserByLogin("login"))
                .willReturn(Mono.error(new UnknownErrorException()));

        // when & then
        StepVerifier.create(authController.authenticateUser(loginRequest, serverWebExchange))
                .expectError(UnknownErrorException.class)
                .verify();

        verify(usersService).getUserByLogin("login");
    }

    @Test
    void shouldReturnJwtWhenPasswordMatching() {
        // given
        User user = User.builder()
                .password("hashCode")
                .build();
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);
        Mono<LoginRequest> loginRequest = Mono.just(new LoginRequest("login", "password"));
        final Date date = new Date();
        Claims claims = mock(Claims.class);

        given(usersService.getUserByLogin("login"))
                .willReturn(Mono.just(user));
        given(passwordEncoder.matches("password", "hashCode"))
                .willReturn(true);
        JwtResponse jwtResponse = JwtResponse.builder()
                .accessToken("accessToken")
                .tokenType("bearer")
                .expiresIn(10L)
                .build();
        given(jwtUtil.generateTokens(user, serverWebExchange))
                .willReturn(
                        Mono.just(jwtResponse)
                );
        given(jwtUtil.getAllClaimsFromToken("token"))
                .willReturn(Mono.just(claims));
        given(claims.getExpiration())
                .willReturn(date);

        // when & then
        StepVerifier.create(authController.authenticateUser(loginRequest, serverWebExchange))
                .expectNextMatches(response -> response.getTokenType().equals("bearer")
                        && response.getExpiresIn().equals(10L)
                        && response.getAccessToken().equals("accessToken")
                )
                .verifyComplete();

        verify(usersService).getUserByLogin("login");
        verify(passwordEncoder).matches("password", "hashCode");
        verify(jwtUtil).generateTokens(user, serverWebExchange);
    }

    @Test
    void shouldExchangeRefreshTokenToAccessToken() {
        // given
        String refreshTokenValue = "refreshToken";
        Map<String, Object> mapOfClaims = new HashMap<>();
        mapOfClaims.put("token_type", REFRESH_TOKEN.name());
        mapOfClaims.put("userId", 1);
        DefaultClaims claims = new DefaultClaims(mapOfClaims);
        User user = User.builder().id(1).build();
        JwtResponse jwtResponse = JwtResponse.builder()
                .tokenType("bearer")
                .accessToken("accessToken")
                .expiresIn(10L)
                .build();
        ServerWebExchange serverWebExchange = mockServerWebExchange(refreshTokenValue);
        given(jwtUtil.getAllClaimsFromToken(refreshTokenValue))
                .willReturn(Mono.just(claims));
        given(usersService.getUserById(1))
                .willReturn(Mono.just(user));
        given(jwtUtil.exchangeRefreshToken(user))
                .willReturn(Mono.just(jwtResponse));

        // when & then
        StepVerifier.create(authController.exchangeToken(serverWebExchange))
                .expectNextMatches(response -> response.getTokenType().equals("bearer")
                        && response.getExpiresIn().equals(10L)
                        && response.getAccessToken().equals("accessToken")
                )
                .verifyComplete();

        verify(jwtUtil).getAllClaimsFromToken(refreshTokenValue);
        verify(usersService).getUserById(1);
        verify(jwtUtil).exchangeRefreshToken(user);
    }

    @Test
    void shouldThrowInvalidTokenExceptionWhenExchangeIncorrectTokenToAccessToken() {
        // given
        String refreshTokenValue = "refreshToken";
        ServerWebExchange serverWebExchange = mockServerWebExchange(refreshTokenValue);
        Map<String, Object> mapOfClaims = new HashMap<>();
        mapOfClaims.put("token_type", ACCESS_TOKEN.name());
        mapOfClaims.put("userId", 1);
        DefaultClaims claims = new DefaultClaims(mapOfClaims);

        given(jwtUtil.getAllClaimsFromToken(refreshTokenValue))
                .willReturn(Mono.just(claims));

        // when & then
        StepVerifier.create(authController.exchangeToken(serverWebExchange))
                .expectError(InvalidJwtTokenException.class)
                .verify();

        verify(jwtUtil).getAllClaimsFromToken(refreshTokenValue);
        verify(usersService, times(0)).getUserById(anyLong());
        verify(jwtUtil, times(0)).exchangeRefreshToken(any());
    }

    @Test
    void shouldThrowIncorrectPasswordExceptionWhenPasswordNotMatching() {
        // given
        User user = User.builder()
                .password("hashCode")
                .build();
        Mono<LoginRequest> loginRequest = Mono.just(new LoginRequest("login", "password"));
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);

        given(usersService.getUserByLogin("login"))
                .willReturn(Mono.just(user));
        given(passwordEncoder.matches("password", "hashCode"))
                .willReturn(false);

        // when & then
        StepVerifier.create(authController.authenticateUser(loginRequest, serverWebExchange))
                .expectError(AuthenticationFailedException.class)
                .verify();

        verify(usersService).getUserByLogin("login");
        verify(passwordEncoder).matches("password", "hashCode");
    }

    @Test
    void shouldCreateUserWhenUserNotExists() {
        // given
        UserDto userDto = new UserDto();
        User user = new User();
        RegisterRequest registerRequest = new RegisterRequest(
                "login",
                "password",
                "email@example.com",
                "John",
                "Smith"
        );

        given(usersService.getUserByLogin("login"))
                .willReturn(Mono.empty());
        given(passwordEncoder.encode("password"))
                .willReturn("encoded password");
        given(userMapper.mapRegisterRequestToUser(registerRequest, "encoded password"))
                .willReturn(user);
        given(usersService.save(user))
                .willReturn(Mono.just(user));
        given(userMapper.mapUserToUserDto(user))
                .willReturn(userDto);

        // when & then
        StepVerifier.create(authController.registerUser(Mono.just(registerRequest)))
                .expectNext(userDto)
                .verifyComplete();

        verify(passwordEncoder).encode("password");
        verify(userMapper).mapRegisterRequestToUser(registerRequest, "encoded password");
        verify(usersService).getUserByLogin("login");
        verify(userMapper).mapUserToUserDto(user);
    }

    @Test
    void shouldThrowExceptionWhenUserAlreadyExist() {
        // given
        User user = new User();
        RegisterRequest registerRequest = new RegisterRequest(
                "login",
                "password",
                "email@example.com",
                "John",
                "Smith"
        );

        given(usersService.getUserByLogin("login"))
                .willReturn(Mono.just(user));

        // when & then
        StepVerifier.create(authController.registerUser(Mono.just(registerRequest)))
                .expectError(UsernameAlreadyTakenException.class)
                .verify();

        verify(usersService).getUserByLogin("login");
    }

    private ServerWebExchange mockServerWebExchange(String desiredCookie) {
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);
        ServerHttpRequest request = mock(ServerHttpRequest.class);
        MultiValueMap<String, HttpCookie> cookies = new LinkedMultiValueMap<>();
        cookies.add("refresh_token", new HttpCookie("refresh_token", desiredCookie));
        given(serverWebExchange.getRequest())
                .willReturn(request);
        given(request.getCookies())
                .willReturn(cookies);
        return serverWebExchange;
    }

}