package com.devleaders.api.config;

import com.devleaders.dataservice.domain.Role;
import com.devleaders.dataservice.domain.RoleType;
import com.devleaders.dataservice.domain.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.test.StepVerifier;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class JwtUtilTest {

    private JwtUtil jwtUtil;

    @BeforeEach
    void beforeEach() {
        String secretKey = "123123123123123123123123123123123123123";
        long accessTokenExpiration = 1800;
        long refreshTokenExpiration = 604800;
        jwtUtil = new JwtUtil(secretKey, accessTokenExpiration, refreshTokenExpiration);
    }

    @Test
    void shouldGenerateTokens() {
        // given
        User user = getUser();
        ServerWebExchange exchange = mock(ServerWebExchange.class);
        ServerHttpResponse response = mock(ServerHttpResponse.class);
        given(exchange.getResponse())
                .willReturn(response);

        // then
        StepVerifier.create(jwtUtil.generateTokens(user, exchange))
                .expectNextMatches(token -> assertToken(token.getAccessToken()))
                .verifyComplete();
        verify(response).addCookie(any());
    }

    @Test
    void shouldExchangeRefreshTokenToAccessToken() {
        // given
        User user = getUser();
        String refreshToken = "eyJhbGciOiJIUzI1NiJ9.eyJmaXJzdE5hbWUiOiJhZG1pbk5hbWUiLCJsYXN0TmFtZSI6ImFkbWluU3VybmFtZSIsInJvbGVzIjpbIlJPTEVfQURNSU4iXSwidG9rZW5fdHlwZSI6IlJFRlJFU0hfVE9LRU4iLCJ1c2VySWQiOjMsImVtYWlsIjoiYWRtaW5AY29tLnBsIiwic3ViIjoiYWRtaW4ifQ._u5f8PtFb9YDv03HBQZYlrDasA3JVrAU6vuIWloiqFk";

        // then
        StepVerifier.create(jwtUtil.exchangeRefreshToken(user))
                .expectNextMatches(token -> assertToken(token.getAccessToken()))
                .verifyComplete();
    }

    private boolean assertToken(String token) {
        Claims claims = Jwts.parserBuilder()
                .setSigningKey("123123123123123123123123123123123123123".getBytes())
                .build()
                .parseClaimsJws(token)
                .getBody();
        List<String> roles = (List<String>) claims.get("roles");
        Integer userId = (Integer) claims.get("userId");
        String firstName = (String) claims.get("firstName");
        String lastName = (String) claims.get("lastName");
        String email = (String) claims.get("email");
        assertEquals("username", claims.getSubject());
        assertEquals(roles.get(0), RoleType.ROLE_USER.name());
        assertEquals(1, userId);
        assertEquals("Brajanusz", firstName);
        assertEquals("Motyka", lastName);
        assertEquals("brajanusz.motyka@email.com", email);
        return true;
    }

    private User getUser() {
        Role role = new Role();
        role.setRoleType(RoleType.ROLE_USER);
        return User.builder()
                .id(1L)
                .login("username")
                .firstName("Brajanusz")
                .lastName("Motyka")
                .email("brajanusz.motyka@email.com")
                .roles(Collections.singletonList(role))
                .build();
    }

}