package com.devleaders.api.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.impl.DefaultClaims;
import io.jsonwebtoken.impl.DefaultHeader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.devleaders.api.config.TokenType.ACCESS_TOKEN;
import static com.devleaders.api.config.TokenType.REFRESH_TOKEN;
import static org.mockito.BDDMockito.given;

class AuthenticationManagerTest {

    @Mock
    private Authentication authentication;

    @Mock
    private JwtUtil jwtUtil;

    @InjectMocks
    private AuthenticationManager authenticationManager;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldSuccessWhenValidToken() {
        // given
        Map<String, Object> variables = new HashMap<>();
        variables.put("sub", "username");
        variables.put("token_type", ACCESS_TOKEN.name());
        variables.put("roles", Collections.emptyList());
        Claims claims = new DefaultClaims(variables);
        given(authentication.getCredentials())
                .willReturn("token");
        given(jwtUtil.getAllClaimsFromToken("token"))
                .willReturn(Mono.just(claims));
        Authentication expectedAuthentication = new UsernamePasswordAuthenticationToken(
                "username",
                "token",
                Collections.emptyList()
        );

        // then
        StepVerifier.create(authenticationManager.authenticate(authentication))
                .expectNext(expectedAuthentication)
                .verifyComplete();
    }

    @Test
    void shouldReturnEmptyWhenRefreshToken() {
        // given
        Map<String, Object> variables = new HashMap<>();
        variables.put("token_type", REFRESH_TOKEN.name());
        Claims claims = new DefaultClaims(variables);
        given(authentication.getCredentials())
                .willReturn("token");
        given(jwtUtil.getAllClaimsFromToken("token"))
                .willReturn(Mono.just(claims));

        // then
        StepVerifier.create(authenticationManager.authenticate(authentication))
                .verifyComplete();
    }


    @Test
    void shouldFailOnExpiredToken() {
        // given
        given(authentication.getCredentials())
                .willReturn("token");
        given(jwtUtil.getAllClaimsFromToken("token"))
                .willReturn(
                        Mono.error(
                                new ExpiredJwtException(
                                        new DefaultHeader(),
                                        new DefaultClaims(Collections.emptyMap()),
                                        ""
                                )
                        )
                );

        // then
        StepVerifier.create(authenticationManager.authenticate(authentication))
                .expectError(ExpiredJwtException.class)
                .verify();
    }

}