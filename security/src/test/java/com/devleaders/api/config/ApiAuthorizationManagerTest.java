package com.devleaders.api.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.server.authorization.AuthorizationContext;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.BDDMockito.given;

class ApiAuthorizationManagerTest {

    @Mock
    private JwtUtil jwtUtil;

    @Mock
    private AuthorizationContext authorizationContext;

    @InjectMocks
    private ApiAuthorizationManager apiAuthorizationManager;

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shoutReturnPositiveAuthorizationDecisionWhenUserIdMatches() {
        // given
        String token = "token";
        Mono<Authentication> authenticationMono = Mono.just(
                new UsernamePasswordAuthenticationToken(
                        "username",
                        token,
                        Collections.emptyList()
                )
        );
        Map<String, Object> authorizationContextVariables = new HashMap<>();
        authorizationContextVariables.put("userId", "2");
        Map<String, Object> authenticationVariables = new HashMap<>();
        authenticationVariables.put("userId", 2);
        Claims claims = new DefaultClaims(authenticationVariables);
        given(authorizationContext.getVariables())
                .willReturn(authorizationContextVariables);
        given(jwtUtil.getAllClaimsFromToken(token))
                .willReturn(Mono.just(claims));

        // then
        StepVerifier.create(apiAuthorizationManager.check(authenticationMono, authorizationContext))
                .expectNextMatches(AuthorizationDecision::isGranted)
                .verifyComplete();
    }

    @Test
    void shoutReturnNegativeAuthorizationDecisionWhenUserIdMatches() {
        // given
        String token = "token";
        Mono<Authentication> authenticationMono = Mono.just(
                new UsernamePasswordAuthenticationToken(
                        "username",
                        token,
                        Collections.emptyList()
                )
        );
        Map<String, Object> authorizationContextVariables = new HashMap<>();
        authorizationContextVariables.put("userId", "1");
        Map<String, Object> authenticationVariables = new HashMap<>();
        authenticationVariables.put("userId", 2);
        Claims claims = new DefaultClaims(authenticationVariables);
        given(authorizationContext.getVariables())
                .willReturn(authorizationContextVariables);
        given(jwtUtil.getAllClaimsFromToken(token))
                .willReturn(Mono.just(claims));

        // then
        StepVerifier.create(apiAuthorizationManager.check(authenticationMono, authorizationContext))
                .expectNextMatches(authorizationDecision -> !authorizationDecision.isGranted())
                .verifyComplete();
    }

}