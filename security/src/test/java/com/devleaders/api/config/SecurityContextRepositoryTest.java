package com.devleaders.api.config;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SecurityContextRepositoryTest {

    @Mock
    private AuthenticationManager authenticationManager;

    @InjectMocks
    private SecurityContextRepository securityContextRepository;

    @Mock
    private ServerWebExchange serverWebExchange;

    @Mock
    private HttpHeaders httpHeaders;

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.initMocks(this);
        ServerHttpRequest serverHttpRequest = mock(ServerHttpRequest.class);

        when(serverWebExchange.getRequest()).thenReturn(serverHttpRequest);
        when(serverHttpRequest.getHeaders()).thenReturn(httpHeaders);
    }

    @Test
    void whenSaveThenException() {
        SecurityContext securityContext = mock(SecurityContext.class);

        UnsupportedOperationException unsupportedOperationException =
                assertThrows(UnsupportedOperationException.class, () -> securityContextRepository.save(serverWebExchange, securityContext));
        assertEquals("Not supported yet", unsupportedOperationException.getMessage());
    }

    @Test
    void whenHeaderDoesNotExistThenEmpty() {
        // given
        given(httpHeaders.containsKey(HttpHeaders.AUTHORIZATION))
                .willReturn(false);

        // then
        StepVerifier.create(securityContextRepository.load(serverWebExchange))
                .verifyComplete();
    }

    @Test
    void whenHeaderDoesNotStartsWithBearerThenEmpty() {
        // given
        given(httpHeaders.containsKey(HttpHeaders.AUTHORIZATION))
                .willReturn(true);
        given(httpHeaders.getFirst(HttpHeaders.AUTHORIZATION))
                .willReturn("jwt-token");

        // then
        StepVerifier.create(securityContextRepository.load(serverWebExchange))
                .verifyComplete();
    }

    @Test
    void whenHeaderValidThenSecurityContext() {
        given(httpHeaders.containsKey(HttpHeaders.AUTHORIZATION))
                .willReturn(true);
        given(httpHeaders.getFirst(HttpHeaders.AUTHORIZATION))
                .willReturn("Bearer jwt-token");
        Authentication expectedAuthentication = new UsernamePasswordAuthenticationToken(
                "username",
                "jwt-token"
        );
        given(authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(null, "jwt-token")))
                .willReturn(Mono.just(expectedAuthentication));

        StepVerifier.create(securityContextRepository.load(serverWebExchange))
                .expectNext(new SecurityContextImpl(expectedAuthentication))
                .verifyComplete();
    }

}