package com.devleaders.api;

import com.devleaders.api.exception.BookNotFoundException;
import com.devleaders.api.exception.UnknownErrorException;
import com.devleaders.dataservice.BookItemService;
import com.devleaders.dataservice.BooksService;
import com.devleaders.dataservice.exception.DataServiceBookNotFoundException;
import com.devleaders.model.Book;
import com.devleaders.model.BookItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDate;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class BookItemControllerTest {

    @Mock
    private BooksService booksService;

    @Mock
    private BookItemService bookItemService;

    @InjectMocks
    private BookItemController bookItemController;

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldReturnBookItemWhenBookFound() {
        // given
        BookItem bookItem = new BookItem(1L, 2L, true, LocalDate.now());
        Book book = Book.builder()
                .id(1L)
                .build();
        given(booksService.getBookById(1))
                .willReturn(Mono.just(book));
        given(bookItemService.getBookItemsForBookId(1))
                .willReturn(Flux.just(bookItem));

        // when & then
        StepVerifier.create(bookItemController.getBookItems(1))
                .expectNext(bookItem)
                .verifyComplete();

        verify(booksService).getBookById(1);
        verify(bookItemService).getBookItemsForBookId(1);
    }


    @Test
    void shouldReturnEmptyStreamWhenNoBookItemsForBook() {
        // given
        Book book = Book.builder()
                .id(1L)
                .build();
        given(booksService.getBookById(1))
                .willReturn(Mono.just(book));
        given(bookItemService.getBookItemsForBookId(1))
                .willReturn(Flux.empty());

        // when & then
        StepVerifier.create(bookItemController.getBookItems(1))
                .verifyComplete();

        verify(booksService).getBookById(1);
        verify(bookItemService).getBookItemsForBookId(1);
    }

    @Test
    void shouldThrowBookNotFoundExceptionWhenBookNotFound() {
        // given
        given(booksService.getBookById(1))
                .willReturn(Mono.empty());

        // when & then
        StepVerifier.create(bookItemController.getBookItems(1))
                .expectErrorMatches(throwable -> throwable instanceof BookNotFoundException
                        && throwable.getMessage().equals("Book not found"))
                .verify();

        verify(booksService).getBookById(1);
        verify(bookItemService, times(0)).getBookItemsForBookId(1);
    }

    @Test
    void shouldThrowUnknownErrorExceptionWhenServiceError() {
        // given
        given(booksService.getBookById(1))
                .willReturn(Mono.error(new BookNotFoundException()));

        // when & then
        StepVerifier.create(bookItemController.getBookItems(1))
                .expectErrorMatches(throwable -> throwable instanceof UnknownErrorException
                        && throwable.getMessage().equals("Oops! Something went wrong!"))
                .verify();

        verify(booksService).getBookById(1);
        verify(bookItemService, times(0)).getBookItemsForBookId(1);
    }

    @Test
    void shouldReturnAvailableBookItemsWhenExists() {
        // given
        BookItem bookItem = new BookItem(1L, 1L, true, LocalDate.now());
        given(bookItemService.getAvailableBookItems(1))
                .willReturn(Flux.just(bookItem));

        // when & then
        StepVerifier.create(bookItemController.getAvailableBookItems(1))
                .expectNext(bookItem)
                .verifyComplete();

        verify(bookItemService, times(1)).getAvailableBookItems(1);
    }

    @Test
    void shouldThrowUnknownErrorExceptionWhenBookItemsServiceError() {
        // given
        given(bookItemService.getAvailableBookItems(1))
                .willReturn(Flux.error(new UnknownErrorException()));

        // when & then
        StepVerifier.create(bookItemController.getAvailableBookItems(1))
                .expectErrorMatches(throwable -> throwable instanceof UnknownErrorException
                        && throwable.getMessage().equals("Oops! Something went wrong!"))
                .verify();

        verify(bookItemService, times(1)).getAvailableBookItems(1);
    }

    @Test
    void shouldThrowBookNotFoundExceptionWhenDataServiceBookNotFoundException() {
        // given
        given(bookItemService.getAvailableBookItems(1))
                .willReturn(Flux.error(new DataServiceBookNotFoundException()));

        // when & then
        StepVerifier.create(bookItemController.getAvailableBookItems(1))
                .expectErrorMatches(throwable -> throwable instanceof BookNotFoundException
                        && throwable.getMessage().equals("Book not found"))
                .verify();

        verify(bookItemService, times(1)).getAvailableBookItems(1);
    }
}