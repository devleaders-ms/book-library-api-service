package com.devleaders.api;

import com.devleaders.api.exception.UnknownErrorException;
import com.devleaders.api.exception.UserNotFoundException;
import com.devleaders.dataservice.BookItemService;
import com.devleaders.dataservice.RentalsService;
import com.devleaders.dataservice.UsersService;
import com.devleaders.dataservice.domain.User;
import com.devleaders.dataservice.exception.DataServiceUnknownErrorException;
import com.devleaders.model.BookItem;
import com.devleaders.model.BookRental;
import com.devleaders.model.CreateRentalRequest;
import com.devleaders.model.RentAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

class RentalsControllerTest {

    @Mock
    private RentalsService rentalsService;

    @Mock
    private UsersService usersService;

    @Mock
    private BookItemService bookItemService;

    @InjectMocks
    private RentalsController rentalsController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void rentalsBookItemsBookItemIdPost() {
        //given
        BookRental bookRental = new BookRental();
        BookItem bookItem = new BookItem();
        Mono<CreateRentalRequest> rentalRequestWithRentActionBorrow = getCreateRentalRequestWithGivenAction(RentAction.BORROW);
        long bookItemId = 1L;
        long userId = 1L;

        given(bookItemService.changeBookItemAvailability(bookItemId))
                .willReturn(Mono.just(bookItem));

        given(rentalsService.rentBook(bookItemId, userId))
                .willReturn(Mono.just(bookRental));

        // when & then
        StepVerifier.create(rentalsController.getBookRental(rentalRequestWithRentActionBorrow))
                .expectNext(bookRental)
                .verifyComplete();

        verify(bookItemService).changeBookItemAvailability(bookItemId);
        verify(rentalsService).rentBook(bookItemId, userId);
    }

    @Test
    void shouldThrowRuntimeException() {
        // given
        Mono<CreateRentalRequest> rentalRequestWithRentActionReturn = getCreateRentalRequestWithGivenAction(RentAction.RETURN);

        // when & then
        StepVerifier.create(rentalsController.getBookRental(rentalRequestWithRentActionReturn))
                .expectErrorMatches(throwable -> throwable instanceof RuntimeException
                        && throwable.getMessage().equals("Not implemented yet !")
                ).verify();
    }

    @Test
    void shouldThrowUnknownErrorExceptionWhenGetUserErrorWhileGetRentalsByUserId() {
        // given
        given(usersService.getUserById(1))
                .willReturn(Mono.error(new DataServiceUnknownErrorException()));

        // when & then
        StepVerifier.create(rentalsController.getRentalsByUserId(1))
                .expectErrorMatches(throwable -> throwable instanceof UnknownErrorException
                        && throwable.getMessage().equals("Oops! Something went wrong!"))
                .verify();
    }

    @Test
    void shouldThrowUserNotFoundExceptionWhenUserNotFoundWhileGetRentalsByUserId() {
        // given
        given(usersService.getUserById(1))
                .willReturn(Mono.empty());

        // when & then
        StepVerifier.create(rentalsController.getRentalsByUserId(1))
                .expectErrorMatches(throwable -> throwable instanceof UserNotFoundException
                        && throwable.getMessage().equals("User not found"))
                .verify();
    }

    @Test
    void shouldThrowUnknownErrorExceptionWhenGetBookRentalErrorWhileGetRentalsByUserId() {
        // given
        User user = new User();
        given(usersService.getUserById(1))
                .willReturn(Mono.just(user));
        given(usersService.getBookRentalsByUserId(1))
                .willReturn(Flux.error(new DataServiceUnknownErrorException()));

        // when & then
        StepVerifier.create(rentalsController.getRentalsByUserId(1))
                .expectErrorMatches(throwable -> throwable instanceof UnknownErrorException
                        && throwable.getMessage().equals("Oops! Something went wrong!"))
                .verify();
    }

    @Test
    void shouldReturnBookRentalWhenGetRentalsByUserIdSuccessful() {
        // given
        User user = new User();
        BookRental bookRental = new BookRental();
        given(usersService.getUserById(1))
                .willReturn(Mono.just(user));
        given(usersService.getBookRentalsByUserId(1))
                .willReturn(Flux.just(bookRental));

        // when & then
        StepVerifier.create(rentalsController.getRentalsByUserId(1))
                .expectNext(bookRental)
                .verifyComplete();
    }

    Mono<CreateRentalRequest> getCreateRentalRequestWithGivenAction(RentAction rentAction) {
        return Mono.just(CreateRentalRequest.builder()
                .bookItemId(1L)
                .userId(1L)
                .type(rentAction)
                .build());
    }
}