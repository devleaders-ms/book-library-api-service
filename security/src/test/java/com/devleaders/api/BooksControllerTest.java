package com.devleaders.api;


import com.devleaders.api.exception.BookNotFoundException;
import com.devleaders.api.exception.UnknownErrorException;
import com.devleaders.dataservice.BooksService;
import com.devleaders.model.Book;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.BDDMockito.given;

class BooksControllerTest {
    @Mock
    private BooksService booksService;

    @InjectMocks
    private BooksController booksController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldGetAllBooksWhenNeitherTitleNorAuthorGiven() {
        // given
        Book book1 = new Book(1L, "The Master and Margarita", "0-14-118014-5",
                "The devil visits atheistic Soviet Union", "fantasy, modernist", 392, "img", null);
        Book book2 = new Book(2L, "The Plague", "978-83-06-03377-9", "Story of a plague sweeping the city of Oran.",
                "philosophical novel", 298, "img", null);

        given(booksService.getAllBooks())
                .willReturn(Flux.just(book1, book2));

        // when && then
        Flux<Book> books = booksController.getBooks(null, null);
        StepVerifier.create(books)
                .expectNext(book1)
                .expectNext(book2)
                .verifyComplete();
    }

    @Test
    void shouldGetBooksByTitleWhenTitleGiven() {

        //given
        Book book1 = new Book(1L, "The Master and Margarita", "0-14-118014-5",
                "The devil visits atheistic Soviet Union", "fantasy, modernist", 392, "img", null);
        String titleName = "The Master and Margarita";

        given(booksService.getBooksByTitleLike(titleName))
                .willReturn(Flux.just(book1));

        // when && then
        Flux<Book> books = booksController.getBooks(titleName, null);
        StepVerifier.create(books)
                .expectNext(book1)
                .verifyComplete();
    }

    @Test
    void shouldGetBooksByTitleWhenAuthorGiven() {
        // given
        Book book1 = new Book(1L, "The Master and Margarita", "0-14-118014-5",
                "The devil visits atheistic Soviet Union", "fantasy, modernist", 392, "img", null);
        String authorName = "Mikhail Bulgakov";

        given(booksService.getBooksByAuthor(authorName))
                .willReturn(Flux.just(book1));

        // when && then
        Flux<Book> books = booksController.getBooks(null, authorName);
        StepVerifier.create(books)
                .expectNext(book1)
                .verifyComplete();

    }

    @Test
    void shouldGetBooksByTitleWhenTitleAndAuthorGiven() {
        // given
        Book book1 = new Book(1L, "The Master and Margarita", "0-14-118014-5",
                "The devil visits atheistic Soviet Union", "fantasy, modernist", 392, "img", null);
        String authorName = "Mikhail Bulgakov";
        String titleName = "The Master and Margarita";

        given(booksService.getBooksByAuthorAndTitle(authorName, titleName))
                .willReturn(Flux.just(book1));

        // when && then
        Flux<Book> books = booksController.getBooks(titleName, authorName);
        StepVerifier.create(books)
                .expectNext(book1)
                .verifyComplete();

    }

    @Test
    void shouldGetBookWhenBookExist() {
        // given
        Book book = new Book();
        given(booksService.getBookById(1L))
                .willReturn(Mono.just(book));

        // when && then
        Mono<Book> bookMono = booksController.getBook(1);
        StepVerifier.create(bookMono)
                .expectNext(book)
                .verifyComplete();
    }

    @Test
    void shouldGetBookNotFoundExceptionWhenBookDoesNotExist() {
        // given
        given(booksService.getBookById(1L))
                .willReturn(Mono.empty());

        // when && then
        Mono<Book> bookMono = booksController.getBook(1);
        StepVerifier.create(bookMono)
                .expectErrorMatches(throwable -> throwable instanceof BookNotFoundException
                        && throwable.getMessage().equals("Book not found"))
                .verify();
    }

    @Test
    void shouldGetUnknownErrorExceptionWhenServerError() {
        // given
        given(booksService.getBookById(1L))
                .willReturn(Mono.error(new UnknownErrorException()));

        // when && then
        Mono<Book> bookMono = booksController.getBook(1);
        StepVerifier.create(bookMono)
                .expectErrorMatches(throwable -> throwable instanceof UnknownErrorException
                        && throwable.getMessage().equals("Oops! Something went wrong!"))
                .verify();
    }
}